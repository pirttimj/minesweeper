### Vera++ code analysis

macro(vera SOURCES TARGET)
    if(VERA++_FOUND)
        find_program(VERAPP vera++)
        add_custom_target(
            ${TARGET}
            COMMAND ${VERAPP}
            --root ${VERA++_DIR}
            --show-rule
            --parameter max-line-length=72
            ${SOURCES}
        )
    else()
        message(
            WARNING
            "Could not find ${VERAPP} -- ignoring code style analysis."
        )
    endif()
endmacro()
