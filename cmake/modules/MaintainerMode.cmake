### Setup Maintainer mode

### Add Maintainer mode compiler flags
# Maintainer mode comes with warnings enabled
set(MAINTAINER_FLAGS
    -Wall
    -pedantic
    -Wextra
    -Wformat=2
    -Wformat-security
    -Wformat-y2k
    -Wmissing-include-dirs
    -Wswitch-default
    -Wswitch-enum
    -Wunused
    -Wfloat-equal
    -Wundef
    -Wunsafe-loop-optimizations
    -Wcast-qual
    -Wcast-align
    -Wconversion
    -Wredundant-decls
    -Winvalid-pch
    -Wlogical-op
#    -Wstack-protector
    -Wshadow
    -Winline
    -Wuninitialized
    -Winit-self
    -Wdisabled-optimization
    -Wpadded
    -Wparentheses
    -Wsign-conversion
    -Wnon-virtual-dtor
    -Wctor-dtor-privacy
    -Wstrict-null-sentinel
    -Woverloaded-virtual
    -Weffc++
)

# Remove semicolons from MAINTAINER_FLAGS list
string(REPLACE ";" " " MAINTAINER_FLAGS "${MAINTAINER_FLAGS}")

# Adding Maintainer mode
set(
    CMAKE_CXX_FLAGS_MAINTAINER
    "${MAINTAINER_FLAGS} ${CXX_FLAGS_MAINTAINER}"
    CACHE STRING
    "Flags used by the C++ compiler during maintainer builds."
    FORCE
)
set(
    CMAKE_EXE_LINKER_FLAGS_MAINTAINER
    "-Wl,--warn-unresolved-symbols,--warn-once"
    CACHE STRING
    "Flags used for linking binaries during mantainer builds."
    FORCE
)
set(
    CMAKE_SHARED_LINKER_FLAGS_MAINTAINER
    "-Wl,--warn-unresolved-symbols,--warn-once"
    CACHE STRING
    "Flags used by the shared libraries linker during maintainer builds."
    FORCE
)
mark_as_advanced(
    CMAKE_CXX_FLAGS_MAINTAINER
    CMAKE_EXE_LINKER_FLAGS_MAINTAINER
    CMAKE_SHARED_LINKER_FLAGS_MAINTAINER
)
# Update the documentation string of CMAKE_BUILD_TYPE for GUIs
set(
    CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}"
    CACHE STRING
    "Choose the type of build, options are: None, Debug, Release, RelWithDebInfo, MinSizeRel, Maintainer."
    FORCE
)
