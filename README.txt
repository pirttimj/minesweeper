Classic MineSweeper Game in Qt/QML/C++.

====================
Building Minesweeper
====================

Build Minesweeper with CMake or Qt Creator.

CMake Build Options
===================

Maintainer - New CMake build type
doc - Generate target doc for Doxygen documentation
vera++ - Source code analysis with Vera++
vera++-test - Unit test code analysis with Vera++

Set CMAKE_PREFIX_PATH to point to the Qt install directory to build the
game with CMake and Qt.

examples:

$ mkdir build
$ cd build

$ cmake -DCMAKE_PREFIX_PATH=/<Qt>/<install>/<directory>

$ cmake -DCMAKE_BUILD_TYPE=Maintainer
$ make

$ cmake -Ddoc=ON
$ make doc

$ cmake -Dtests=ON
$ make vera++
$ make vera++-test

Building with Qt Creator
========================

Open and build project quick2.pro in directory ui/quick2

=======
Testing
=======

Library Minesweeper tests
=========================

$ mkdir build
$ cd build

# Configure
$ cmake ..
$ make tests

# Run tests
$ ./lib/tests/tests

QML Quick 2 tests
=================

qtcreator:

Open and run project Tests in directory ui/quick2/tests

cmake:

$ mkdir build
$ cd build

# Configure
$ cmake ..
$ make qmltests

# Run tests
$ ./ui/quick2/tests/qmltests -input <test>/<directory>

examples:

# Run all tests
$ ./ui/quick2/tests/qmltests -input ../ui/quick2/tests

# Run all tests from a directory
$ ./ui/quick2/tests/qmltests -input ../ui/quick2/tests/Tile

# Run single test
$ ./ui/quick2/tests/qmltests -input ../ui/quick2/tests/Tile/tst_tile_reset.qml
