// Group definitions
//!
//! @defgroup ui QML Quick2 Components
//! @brief QML Quick2 Components for UI.
//!
//  ====================================================================
//!
//! @defgroup uitests QML Quick2 UI Tests
//! @brief QML Quick2 UI Unit Tests
//!
//  ====================================================================
//!
//! @defgroup tiletests Tile
//! @brief Component Tile UI Tests
//!
//! @ingroup uitests
//!
//  ====================================================================
//!
//! @defgroup tile Tile
//! @brief Tile Component.
//!
//! Tile represents a single tile on board. The tile may be in one of
//! the following states: UNKNOWN, FLAGGED, or REVEALED. See
//! {@link MS::Minesweeper::States states} for more information.
//!
//! @ingroup ui
//!
//  ====================================================================
//!
//! @defgroup board Board
//! @brief Board Component
//!
//! Board represents a grid of tiles (game board). Board grid consists
//! of NxM rows and columns.
//!
//! @ingroup ui
//!
//  ====================================================================
//!
//! @defgroup boardtests Board
//! @brief Component Board UI Tests
//!
//! @ingroup uitests
//!
//  ====================================================================
//!
//! @defgroup settings Settings
//! @brief Settings Component
//!
//! Settings view to modify game settings. Modifiable parameters:
//!
//! <table>
//! <tr><th>Parameter</th><th>Description</th></tr>
//! <tr><td>rows</td><td>Number of rows</td></tr>
//! <tr><td>cols</td><td>Number of cols</td></tr>
//! <tr><td>mines</td><td>Number of mines</td></tr>
//! </table>
//!
//! The view contains three button groups for quick selection and
//! one for custom settings selection.
//!
//! <table>
//! <tr><th>%Board Size</th><th>Difficulty Level</th><th>Rows</th>
//!     <th>Columns</th><th>Mines</th></tr>
//! <tr><td rowspan="3">Small</td><td>Easy</td><td rowspan="3">8</td>
//!     <td rowspan="3">8</td><td>10</td></tr>
//! <tr><td>Intermediate</td><td>12</td></tr>
//! <tr><td>Difficult</td><td>14</td></tr>
//! <tr><td rowspan="3">Medium</td><td>Easy</td><td rowspan="3">16</td>
//!     <td rowspan="3">16</td><td>40</td></tr>
//! <tr><td>Intermediate</td><td>48</td></tr>
//! <tr><td>Difficult</td><td>56</td></tr>
//! <tr><td rowspan="3">Large</td><td>Easy</td><td rowspan="3">16</td>
//!     <td rowspan="3">30</td><td>75</td></tr>
//! <tr><td>Intermediate</td><td>90</td></tr>
//! <tr><td>Difficult</td><td>105</td></tr>
//! <tr><td>Custom</td><td>Custom</td><td>2 - 99</td><td>2 - 99</td>
//!     <td>1 - (rows&times;cols-1)</td>
//! </tr>
//! </table>
//!
//! @ingroup ui
//!
//  ====================================================================
//!
//! @defgroup settingstests Settings
//! @brief Component Settings UI Tests
//!
//! @ingroup uitests
//!
//  ====================================================================
//!
//! @defgroup statusBar StatusBar
//! @brief StatusBar Component
//!
//! Game status info: Flag counter, New Game button, and Clock.
//!
//! @ingroup ui
//!
//  ====================================================================
//!
//! @defgroup flagCounter FlagCounter
//! @brief FlagCounter Component
//!
//! Flag Counter
//!
//! @ingroup statusBar
//!
//  ====================================================================
//!
//! @defgroup newGameButton NewGameButton
//! @brief NewGameButton Component
//!
//! New Game button
//!
//! @ingroup statusBar
//!
//  ====================================================================
//!
//! @defgroup clock Clock
//! @brief Clock Component
//!
//! Timer
//!
//! @ingroup statusBar
