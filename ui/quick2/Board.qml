/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file Board.qml
//! @brief Component Board functionality.

import QtQuick 2.4

//! @class Board
//! @brief Component Board functionality.
//! @addtogroup board
//! @{
BoardForm {
    Component.onCompleted: {
        // Connection to Settings::newGame signal
        root.menuBar.newGame.connect(newGame);

        rows = Qt.binding(function() {
            return root.rows;
        });
        cols = Qt.binding(function() {
            return root.cols;
        });
    }

    Component.onDestruction: {
        // Disconnect from signal Settings::newGame
        root.menuBar.newGame.disconnect(newGame);
    }

    //! @brief Reset board.
    //!
    //! Reset all board tiles to state {@link MS::Minesweeper::States
    //! UNKNOWN}.
    function reset() {
        for (var i=0; i<tiles.count; i++) {
            tiles.itemAt(i).reset();
        }
    }

    //! @brief Start a new game.
    //!
    //! Setup board for a new game
    function newGame() {
        reset();
    }

    //! @brief Finish the game.
    //!
    //! Disable tiles.
    function finished() {
        for (var i=0; i<tiles.count; i++) {
            tiles.itemAt(i).enabled = false;
        }
    }

    Connections {
        target: MSWrapper;

        onReveal: {
            tiles.itemAt(location).reveal(adjacentMines);
        }

        onHit: {
            tiles.itemAt(location).hit();
            finished();
        }

        onMine: {
            tiles.itemAt(location).state = "MINE";
        }

        onFalseFlag: {
            tiles.itemAt(location).state = "FALSEFLAG";
        }

        onFinished: {
            finished();
        }

        onPaused: {
            if (MSWrapper.isRunning()) {
                tiles.parent.opacity = (paused) ? 0 : 1;
                tiles.parent.enabled = !paused;
            }
        }
    }
}
//! @}
