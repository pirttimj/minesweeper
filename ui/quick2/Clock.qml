/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file Clock.qml
//! @brief Component Clock functionality.

import QtQuick 2.4

//! @class Clock
//! @brief Clock functionality
//! @addtogroup clock
//! @{
ClockForm {
    //! @brief type:Timer Reference to Component Timer.
    property alias ticker: ticker;
    //! @brief type:Item Reference to loaded Item seconds.
    property alias ss: seconds.item;
    //! @brief type:Item Reference to loaded Item minutes.
    property alias mm: minutes.item;
    //! @brief type:Item Reference to loaded Item hours.
    property alias hh: hours.item;

    timer.text: ((hh.count < 10) ? "0" + hh.count : hh.count)
        + ":"
        + ((mm.count < 10) ? "0" + mm.count : mm.count)
        + ":"
        + ((ss.count < 10) ? "0" + ss.count : ss.count);

    Timer {
        id: ticker;
        interval: 1000;
        repeat: true;
        running: false;

        onTriggered: {
            seconds.item.increase();
        }
    }

    mouseArea.onClicked: {
        if (MSWrapper.isRunning())
        {
            MSWrapper.togglePause();
        }
    }

    Component {
        id: counter;

        Item {
            //! @brief type:int Counter
            property int count;
            //! @brief type:int Counter minimum value
            property int minimum;
            //! @brief type:int Counter maximum value
            property int maximum;
            //! @brief Overflow signal
            signal overflow();

            function increase() {
                if (count < maximum) {
                    count = count + 1;
                } else {
                    count = minimum;
                    overflow();
                }
            }
        }
    }

    Loader {
        id: seconds;
        sourceComponent: counter;

        onLoaded: {
            seconds.item.count = 0;
            seconds.item.minimum = 0;
            seconds.item.maximum = 59;
        }
    }

    Loader {
        id: minutes;
        sourceComponent: counter;

        onLoaded: {
            minutes.item.count = 0;
            minutes.item.minimum = 0;
            minutes.item.maximum = 59;
        }

        Connections {
            target: seconds.item;

            onOverflow: {
                minutes.item.increase();
            }
        }
    }

    Loader {
        id: hours;
        sourceComponent: counter;

        onLoaded: {
            hours.item.count = 0;
            hours.item.minimum = 0;
            hours.item.maximum = 23;
        }

        Connections {
            target: minutes.item;

            onOverflow: {
                hours.item.increase();
            }
        }
    }

    Connections {
        target: MSWrapper

        onStart: {
            ticker.start();
        }

        onHit: {
            ticker.stop();
        }

        onFinished: {
            ticker.stop();
        }

        onPaused: {
            ticker.running = !paused;
        }
    }
}
//! @}
