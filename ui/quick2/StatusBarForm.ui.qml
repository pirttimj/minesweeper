/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file StatusBarForm.ui.qml
//! @brief Minesweeper Status Bar UI form.

import QtQuick 2.4
import QtQuick.Layouts 1.3

//! @class StatusBarForm
//! @brief Minesweeper StatusBar UI form.
//!
//! @addtogroup ui
//! @{
Item {
    height: 48
    //! @brief New Game Button
    property alias newGameButton: newGame

    Rectangle {
        id: background
        anchors.fill: parent
        color: "darkgray"

        RowLayout {
            anchors.fill: parent

            FlagCounter {
                id: flagCounter
                Layout.margins: 8
            }

            NewGameButton {
                id: newGame
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.margins: 4
            }

            Clock {
                id: clock
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.margins: 8
            }
        }
    }
}
//! @}
