/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file TileForm.ui.qml
//! @brief Component Tile UI form.

import QtQuick 2.4

//! @class TileForm
//! @brief Component Tile UI Form.
//! @addtogroup tile Tile
//! @{
Item {
    id: tileItem
    width: 32
    height: 32

    //! type:Rectangle Tile background rectangle
    property alias tileBackground: background
    //! type:Image Tile image
    property alias tileImage: image
    //! type:MouseArea Tile mouse area
    property alias tileMouseArea: mouseArea

    Rectangle {
        id: background
        color: "lightgray"
        anchors.fill: parent
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Image {
        id: image
        anchors.fill: parent
        source: "qrc:/images/Empty"
    }

    states: [
        State {
            name: "UNKNOWN";

            PropertyChanges {
                target: form.tileBackground;
                color: "lightgray";
            }

            PropertyChanges {
                target: form.tileMouseArea;
                enabled: true;
            }

            PropertyChanges {
                target: form.tileImage;
                source: "qrc:/images/Empty";
            }
        },
        State {
            name: "FLAGGED";

            PropertyChanges {
                target: form.tileImage;
                source: "qrc:/images/Flag";
            }
        },
        State {
            name: "REVEALED";

            PropertyChanges {
                target: form.tileBackground;
                color: "darkgray";
            }

            PropertyChanges {
                target: form.tileMouseArea;
                enabled: false;
            }
        },
        State {
            name: "MINE";

            PropertyChanges {
                target: form.tileImage;
                source: "qrc:/images/Mine";
            }

            PropertyChanges {
                target: form.tileBackground;
                color: "lightgray";
            }
        },
        State {
            name: "FALSEFLAG";

            PropertyChanges {
                target: form.tileImage;
                source: "qrc:/images/FalseFlag";
            }

            PropertyChanges {
                target: form.tileBackground;
                color: "lightgray";
            }
        }
    ]
}
//! @}
