/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file mswrapper.cpp

#include "mswrapper.h"

MSWrapper::MSWrapper(QObject *parent) : QObject(parent)
{
    ms_ = new MS::Minesweeper();
}

void MSWrapper::setup(int rows, int cols, int mines)
{
    rows_ = rows;
    cols_ = cols;
    mines_ = mines;
    ms_->setup(rows, cols, mines);
    running_ = false;
    paused_ = false;
}

MSWrapper::States MSWrapper::toggleFlag(int index)
{
    MS::Minesweeper::States state = ms_->toggleFlag(index);

    // Emit flag count
    emit flags(ms_->getFlags().size());

    return static_cast<MSWrapper::States>(static_cast<int>(state));
}

void MSWrapper::select(unsigned int index)
{
    // Beginning of the game
    if (!running_)
    {
        ms_->setMines(mines_, index);
        emit start();
        running_ = true;
    }

    if (ms_->isMined(index))
    {
        // End of game; reveal mine locations and mark false flags
        for (int i=0; i<rows_*cols_; ++i)
        {
            bool isMined = ms_->isMined(i);

            if (ms_->getState(i) == MS::Minesweeper::FLAGGED)
            {
                if (!isMined)
                {
                    emit falseFlag(i);
                }
            }
            else
            {
                if (isMined)
                {
                    emit mine(i);
                }
            }
        }

        emit hit(index);
        running_ = false;

        return;
    }

    std::vector<unsigned int> locations = ms_->select(index);

    for (auto const& location : locations)
    {
        emit reveal(location, ms_->getAdjacentMineCount(location));
    }

    if (ms_->isFinished())
    {
        emit finished();
        running_ = false;
    }
}

bool MSWrapper::isMined(unsigned int index) const
{
    return ms_->isMined(index);
}

bool MSWrapper::isFlagged(unsigned int index) const
{
    return ms_->getState(index) == MS::Minesweeper::FLAGGED;
}

bool MSWrapper::isRunning() const
{
    return running_;
}

void MSWrapper::togglePause()
{
    paused_ = !paused_;
    emit paused(paused_);
}
