/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file mswrapper.h
//! @brief Wrapper class for Minesweeper library class.

#ifndef MSWRAPPER_H
#define MSWRAPPER_H

#include "minesweeper.h"
#include <QObject>

//! @class MSWrapper
//! @brief Wrapper class for C++/QML integration.
//!
//! Exposing library Minesweeper methods to QML code.
class MSWrapper : public QObject
{
    Q_OBJECT
    Q_ENUMS(States)

public:
    //! @brief Square states.
    //!
    //! See {@link MS::Minesweeper::States States}.
    enum States {
        UNKNOWN,
        REVEALED,
        FLAGGED
    };

    //! @brief Constructor.
    explicit MSWrapper(QObject *parent = 0);

    //! @brief Game setup.
    //!
    //! Setup a new game. Set rows, columns, and mines. See
    //! {@link MS::Minesweeper::setup setup} for details.
    //!
    //! @param rows Number of rows
    //! @param cols Number of columns
    //! @param mines Number of mines
    Q_INVOKABLE void setup(int rows, int cols, int mines);

    //! @brief Toggle flag state.
    //!
    //! Toggle flag state between {@link MS::Minesweeper::UNKNOWN
    //! UNKNOWN} and {@link MS::Minesweeper::FLAGGED FLAGGED}. Does
    //! nothing if current state is {@link MS::Minesweeper::REVEALED
    //! REVEALED}. See function {@link MS::Minesweeper::toggleFlag()}.
    //!
    //! @param index Location index
    //! @return Minesweeper library flag state as integer
    Q_INVOKABLE States toggleFlag(int index);

    //! @brief Select given location.
    //!
    //! @param index Selected location
    Q_INVOKABLE void select(unsigned int index);

    //! @brief Is current location mined.
    //!
    //! @param index Location index
    //! @return True, if selected location contains a mine, false
    //! otherwise
    Q_INVOKABLE bool isMined(unsigned int index) const;

    //! @brief Is current location flagged.
    //!
    //! @param index Location index
    //! @return True, if selected location contains a flag, false
    //! otherwise
    Q_INVOKABLE bool isFlagged(unsigned int index) const;

    //! @brief Get game running state
    //!
    //! @return True, if game is running, false otherwise (game has not
    //! been started yet or is already ended).
    Q_INVOKABLE bool isRunning() const;

    //! @brief Toggle game pause state.
    Q_INVOKABLE void togglePause();

signals:
    //! @brief Start of the game.
    //!
    //! Emit at the beginning of the game.
    void start();

    //! @brief Disclose tile data.
    //!
    //! Returns tile data: tile location index and number of adjacent
    //! mines for that tile.
    //!
    //! @param location Tile location index
    //! @param adjacentMines Adjacent mine count for tile
    void reveal(unsigned int location, unsigned int adjacentMines);

    //! @brief Hit a mined tile.
    //!
    //! Emit if selected tile is mined.
    //!
    //! @param location Mine location
    void hit(unsigned int location);

    //! @brief Flag count.
    //!
    //! Emit when flags are added or removed.
    //!
    //! @param flagCount Number of flags
    void flags(unsigned int flagCount);

    //! @brief Mine location.
    //!
    //! Emit at the end of the game to reveal mine locations.
    //!
    //! @param location Mine location
    void mine(unsigned int location);

    //! @brief False flag.
    //!
    //! Emit at the end of the game to mark false flags.
    //!
    //! @param location False flag location
    void falseFlag(unsigned int location);

    //! @brief Game paused.
    //!
    //! Emit when game pause state changes.
    //!
    //! @param paused True, if game paused, false otherwise.
    void paused(bool paused);

    //! @brief Game finished.
    //!
    //! Emit when game ends.
    void finished();

private:
    int rows_;
    int cols_;
    int mines_;
    MS::Minesweeper *ms_;
    bool running_;
    bool paused_;

};

#endif // MSWRAPPER_H
