/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file SelectionButtonGroupForm.ui.qml
//! @brief Component SelectionButtonGroup UI form.

import QtQuick 2.4
import QtQuick.Layouts 1.3

//! @class SelectionButtonGroupForm
//! @brief SelectionButtonGroup UI form.
//!
//! Selection button group form. Buttons for preconfigured game
//! settings. Each group (Small, Medium, and Large) contains three
//! buttons (Easy, Intermediate, and Difficult) for quick start.
//!
//! @addtogroup settings
//! @{
Item {
    Layout.fillHeight: true;
    Layout.fillWidth: true;

    //! @brief Selection button group title
    property alias label: label.text

    //! @brief Selection signal
    //!
    //! @param size %Board size string
    //! @param level Game difficulty level
    signal selection(string size, string level);

    Rectangle {
        id: background;
        color: "transparent";
        anchors.fill: parent;
    }

    ColumnLayout {
        id: columnLayout;
        anchors.topMargin: 10;
        anchors.bottomMargin: 10;
        anchors.rightMargin: 10;
        anchors.leftMargin: 10;
        anchors.fill: parent;

        Text {
            id: label;
            text: qsTr("Size");
            horizontalAlignment: Text.AlignHCenter;
            Layout.fillWidth: true;
            font.pixelSize: 12;
        }

        SelectionButton {
            id: selectionEasy;
            objectName: "selectionEasy";
            Layout.fillWidth: true;
            size: label.text;
            level: "Easy";
        }

        SelectionButton {
            id: selectionIntermediate;
            objectName: "selectionIntermediate";
            Layout.fillWidth: true;
            size: label.text;
            level: "Intermediate";
        }

        SelectionButton {
            id: selectionDifficult;
            objectName: "selectionDifficult";
            Layout.fillWidth: true;
            size: label.text;
            level: "Difficult";
        }
    }
}
//! @}
