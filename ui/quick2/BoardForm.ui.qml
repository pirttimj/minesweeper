/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file BoardForm.ui.qml
//! @brief Component Board UI form.

import QtQuick 2.4
import QtQuick.Layouts 1.3

//! @class BoardForm
//! @brief Component Board UI Form.
//! @addtogroup board Board
//! @{
Item {
    id: form;

    //! type:Tile List of tiles
    property alias tiles: tiles;
    //! type:int Number of rows on board
    property int rows;
    //! type:int Number of columns on board
    property int cols;

    GridLayout {
        anchors.centerIn: parent;
        rows: rows;
        columns: cols;
        rowSpacing: 1;
        columnSpacing: 1;

        Repeater {
            id: tiles;
            anchors.fill: parent;
            model: rows*cols;

            Tile {
                id: tile;
            }
        }
    }
}
//! @}
