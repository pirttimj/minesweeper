/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file ClockForm.ui.qml
//! @brief Component Clock UI Form.

import QtQuick 2.4
import QtQuick.Layouts 1.3

//! @class ClockForm
//! @brief Clock UI Form.
//!
//! @addtogroup clock
//! @{
Item {
    height: 32
    width: 80
    //! @brief type:Text Clock text field.
    property alias timer: timer
    //! @brief type:MouseArea Clock mouse area
    property alias mouseArea: mouseArea

    //! @brief Game paused
    //!
    //! @param pause true, if paused, false otherwise
    signal paused(bool pause)

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    Rectangle {
        id: background
        height: 32
        width: 80
        color: "lightgray"
        radius: 5
        border.color: "gray"
        border.width: 3

        Text {
            id: timer
            anchors.centerIn: parent
        }
    }

}
//! @}
