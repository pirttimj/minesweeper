/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file NewGameButton.qml
//! @brief Component NewGameButton functionality.

import QtQuick 2.4

//! @class NewGameButton
//! @brief NewGameButton functionality
//! @addtogroup newGameButton
//! @{
NewGameButtonForm {
    mouseArea.onClicked: {
        root.newGame();
    }

    Connections {
        target: MSWrapper;

        onHit: {
            newGameButton.state = "LOSER";
        }

        onReveal: {
            newGameButton.state = "SMILE";
        }
    }

    //! @brief Update New Game Button face image.
    function pressed(index) {
        if (!MSWrapper.isFlagged(index)) {
            newGameButton.state = "SCARED";
        }
    }

    //! @brief Update New Game Button if winning the game.
    function finished() {
        newGameButton.state = "WINNER";
    }
}
//! @}
