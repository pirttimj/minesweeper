TEMPLATE = app

TARGET = minesweeper

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    ../../lib/src/minesweeper.cpp \
    src/mswrapper.cpp

RESOURCES += qml.qrc \
    ../images/images.qrc

# Additional import path used to resolve QML modules in Qt Creator's
# code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ../../lib/src/minesweeper.h \
    src/mswrapper.h

INCLUDEPATH = $$PWD/../../lib/src/
