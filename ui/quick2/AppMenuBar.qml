/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file AppMenuBar.qml
//! @brief Component Menubar

import QtQuick 2.0
import QtQuick.Controls 1.4

//! @class AppMenuBar
//! @brief Component Menubar
//!
//! Menu items and actions
//!
//! <table>
//! <tr><th colspan="3">File (Alt+F)</th></tr>
//! <tr><td>Menu Item</td><td>Shortcut</td><td>Function</td></tr>
//! <tr><td>New Game</td><td>Alt+N</td>
//!     <td>Start a new game using the current game settings:
//!         <ul>
//!             <li>Number of rows</li>
//!             <li>Number of cols</li>
//!             <li>Number of mines</li>
//!         </ul>
//!    </td>
//! </tr>
//! <tr>
//!     <td>%Settings</td><td>Alt+S</td><td>Modify game settings.</td>
//! </tr>
//! <tr><td>Quit</td><td>Alt+Q</td><td>Quit and exit game</td></tr>
//! </table>
//!
//! @addtogroup ui
//! @{
MenuBar {
    id: menu;

    //! @brief Start a new game
    signal newGame();
    //! @brief Open modify settings view
    signal modifySettings();

    // File items
    Menu {
        title: qsTr("&File");

        // New game
        MenuItem {
            text: qsTr("&New Game");

            onTriggered: {
                newGame();
            }
        }

        // Settings
        MenuItem {
            text: qsTr("&Settings");

            onTriggered: {
                modifySettings();
            }
        }

        // Quit game
        MenuItem {
            text: qsTr("&Quit");

            onTriggered: {
                Qt.quit();
            }
        }
    }
}
//! @}
