/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file FlagCounterForm.ui.qml
//! @brief Component FlagCounter UI Form.

import QtQuick 2.4
import QtQuick.Layouts 1.3

//! @class FlagCounterForm
//! @brief FlagCounter UI form.
//!
//! @addtogroup flagCounter
//! @{
Item {
    height: 32
    width: 80

    //! type:int Number of mines on board
    property int mines: 0
    //! type:string Counter text field
    property alias counter: counter
    //! type:Rectangle Flag Counter background rectangle
    property alias background: background

    RowLayout {

        Image {
            id: flag
            height: 32
            width: 32
            source: "qrc:/images/Flag"
        }

        Rectangle {
            id: background
            color: "#ffffff"
            radius: 5
            border.width: 3
            height: 32
            width: 80

            Text {
                id: counter
                anchors.centerIn: parent

            }
        }
    }
}
//! @}
