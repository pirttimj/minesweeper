/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file FlagCounter.qml
//! @brief Component FlagCounter functionality.

import QtQuick 2.4

//! @class FlagCounter
//! @brief FlagCounter functionality
//! @addtogroup flagCounter
//! @{
FlagCounterForm {
    property int flags: 0;
    state: "DEFAULT";

    Component.onCompleted: {
        mines = Qt.binding(function() {
            return root.mines;
        });

        counter.text = flags + "/" + mines;
    }

    Connections {
        target: MSWrapper;

        onFlags: {
            flags = flagCount;
        }
    }

    onFlagsChanged: {
        counter.text = flags + "/" + mines;
    }

    states: [
        State {
            name: "DEFAULT";
            when: flags < mines;

            PropertyChanges {
                target: background;
                border.color: "gray";
                color: "lightgray";
            }
        },

        State {
            name: "EQUAL";
            when: flags === mines;

            PropertyChanges {
                target: background;
                border.color: "green";
                color: "#8000ff00";
            }
        },

        State {
            name: "OVERFLOW";
            when: flags > mines;

            PropertyChanges {
                target: background
                border.color: "red";
                color: "#80ff0000";
            }
        }
    ]
}
//! @}
