/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file Tile.qml
//! @brief Component Tile functionality.

import MSWrapper.Enums 1.0
import QtQuick 2.4

//! @class Tile
//! @brief Component Tile functionality.
//! @addtogroup tile
//! @{
TileForm {
    id: form

    state: "UNKNOWN";
    tileMouseArea.acceptedButtons: Qt.LeftButton | Qt.RightButton;

    tileMouseArea.onReleased: {
        if (mouse.button === Qt.LeftButton) {
            // Left click: Reveal current location
            if (state == "UNKNOWN") {
                MSWrapper.select(index);
            }
        } else {
            // Right click: Toggle flag state
            var flag = MSWrapper.toggleFlag(index);

            if (flag === Enums.FLAGGED) {
                state = "FLAGGED";
            }

            if (flag === Enums.UNKNOWN) {
                state = "UNKNOWN";
            }
        }
    }

    tileMouseArea.onPressed: {
        if (mouse.button === Qt.LeftButton) {
            statusBar.pressed(index);
        }
    }

    //! @brief Reset tile.
    //!
    //! Reset tile to the state {@link MS::Minesweeper::States UNKNOWN}.
    function reset() {
        state = "UNKNOWN";
    }

    //! @brief Mark revealed tiles.
    //!
    //! Set tile state to {@link MS::Minesweeper::States REVEAL} and
    //! set the tile image.
    //!
    //! @param type:int adjacentMines Adjacent mine count for current tile
    function reveal(adjacentMines) {
        state = "REVEALED";
        setTileImage(adjacentMines);
    }

    //! @brief Set tile image for revealed tiles.
    //!
    //! @param type:int adjacentMines Adjacent mine count
    function setTileImage(adjacentMines) {
        switch (adjacentMines) {
        case 0:
            tileImage.source = "qrc:/images/Empty";
            break;
        case 1:
            tileImage.source = "qrc:/images/Number_1";
            break;
        case 2:
            tileImage.source = "qrc:/images/Number_2";
            break;
        case 3:
            tileImage.source = "qrc:/images/Number_3";
            break;
        case 4:
            tileImage.source = "qrc:/images/Number_4";
            break;
        case 5:
            tileImage.source = "qrc:/images/Number_5";
            break;
        case 6:
            tileImage.source = "qrc:/images/Number_6";
            break;
        case 7:
            tileImage.source = "qrc:/images/Number_7";
            break;
        case 8:
            tileImage.source = "qrc:/images/Number_8";
            break;
        case 9:
            // ERROR: Should not happen
            break;
        default:
            // ERROR: Should not happen
            break;
        }
    }

    //! @brief Set tile backround color when hit a mine.
    function hit() {
        tileBackground.color = "red";
    }
}
//! @}
