/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file SettingsForm.ui.qml
//! @brief Component Settings UI form.

import QtQuick 2.4
import QtQuick.Layouts 1.3

//! @class SettingsForm
//! @brief Settings UI form.
//! @addtogroup settings
//! @{
Item {
    id: item1
    anchors.fill: parent;

    //! @brief Game selection
    //!
    //! @param size %Board size
    //! @param level Game level
    //! @param rows Number of rows
    //! @param cols Number of columns
    //! @param mines Number of mines
    signal settings(
        string size, string level, int rows, int cols, int mines
    );

    Rectangle {
        id: background;
        color: "transparent";
        anchors.fill: parent;
    }

    GridLayout {
        id: grid;
        anchors.rightMargin: 0;
        anchors.leftMargin: 0;
        anchors.bottomMargin: 0;
        anchors.topMargin: 0;
        anchors.fill: parent;
        columnSpacing: 0;
        rowSpacing: 0;
        rows: 2;
        columns: 2;

        SelectionButtonGroup {
            id: selectionSmall;
            objectName: "selectionSmall";
            width: parent.width / grid.columns;
            height: parent.height / grid.rows;
            label: "Small";
        }

        SelectionButtonGroup {
            id: selectionMedium;
            objectName: "selectionMedium";
            width: parent.width / grid.columns;
            height: parent.height / grid.rows;
            label: "Medium";
        }

        SelectionButtonGroup {
            id: selectionLarge;
            objectName: "selectionLarge";
            width: parent.width / grid.columns;
            height: parent.height / grid.rows;
            label: "Large";
        }

        CustomSelection {
            id: customSelection;
        }
    }
}
//! @}
