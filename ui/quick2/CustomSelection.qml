/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file CustomSelection.qml
//! @brief Component CustomSelection functionality.

import QtQuick 2.4

//! @class CustomSelection
//! @brief CustomSelection functionality
//! @addtogroup settings
//! @{
CustomSelectionForm {
    mouseArea.onClicked: {
        settings("Custom", "Custom", spinBoxRows.value,
                 spinBoxColumns.value, spinBoxMines.value);
    }

    spinBoxRows.onValueChanged: {
        setMinesUpIndicator();
    }

    spinBoxColumns.onValueChanged: {
        setMinesUpIndicator();
    }

    //! @brief Set SpinBox Mines Up indicator enabled state
    //!
    //! Keep SpinBox Mines Up indicator enabled as long as mines count
    //! is less than maximum mine count.
    function setMinesUpIndicator() {
        var max = spinBoxRows.value*spinBoxColumns.value-1;

        spinBoxMines.up.indicator.enabled = (spinBoxMines.value != max);
    }
}
//! @}
