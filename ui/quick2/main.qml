/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file main.qml
//! @brief Minesweeper QML Quick 2 main window

import QtQuick.Controls 1.4
import QtQuick 2.5
import QtQuick.Window 2.2

//! @class main
//! @brief Minesweeper QML Quick 2 main window
//! @addtogroup ui
//! @{
ApplicationWindow {
    id: root;
    visible: true;
    // Height: Rows + spacing + menubar height + status bar height
    height: rows*32 + (rows+1) + menuBar.__contentItem.height + 48;
    // Width: Columns + spacing width
    width: cols*32 + (cols+1);
    // Minimum height/width: reserve enough space for Settings view
    minimumHeight: 450;
    minimumWidth: 438;
    title: qsTr("Minesweeper");

    onRowsChanged: {
        var h = rows*32 + (rows+1) + menuBar.__contentItem.height + 48;
        minimumHeight = (h < 450) ? 450 : h;
    }

    onColsChanged: {
        var w = cols*32 + (cols+1);
        minimumWidth = (w < 438) ? 438 : w;
    }

    //! %Board size string
    property string size: "Small";
    //! Game difficulty level
    property string level: "Easy";
    //! Number of rows
    property int rows: 2;
    //! Number of columns
    property int cols: 2;
    //! Number of mines
    property int mines: 1;

    menuBar: AppMenuBar {
        id: menuBar;
    }

    Loader {
        id: loader;
        anchors.fill: parent;
    }

    Component.onCompleted: {
        // Initial view: Settings view
        loader.source = "SettingsForm.ui.qml";

        // Connection to AppMenuBar::modifySettings signal
        root.menuBar.onModifySettings.connect(modifySettings);
        // Connection to AppMenuBar::newGame signal
        root.menuBar.onNewGame.connect(root.newGame);
    }

    Connections {
        target: loader.item;

        // Ignore errors from unconnected signals
        ignoreUnknownSignals: true;

        onSettings: {
            root.size = size;
            root.level = level;
            root.rows = rows;
            root.cols = cols;
            root.mines = mines;

            newGame();
        }
    }

    //! @brief Open modify settings view.
    //!
    //! Open board setup view for board parameter configuration
    function modifySettings() {
        loader.source = "SettingsForm.ui.qml";
    }

    //! @brief Start a new game.
    //!
    //! Start a new game using current board settings
    function newGame() {
        loader.source = "";
        loader.source = "MainForm.ui.qml";

        // Board setup
        MSWrapper.setup(rows, cols, mines);
    }
}
//! @}
