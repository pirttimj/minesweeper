/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file SelectionButton.qml
//! @brief Component SelectionButton functionality.
import QtQuick 2.4

//! @class SelectionButton
//! @brief SelectionButton functionality
//! @addtogroup settings
//! @{
SelectionButtonForm {
    mouseArea.onClicked: {
        // size: Selection button size string
        // level: Selection button difficulty level
        selection(size, level);
    }
}
//! @}
