/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "src/mswrapper.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    MSWrapper wrapper;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("MSWrapper", &wrapper);
    qmlRegisterType<MSWrapper>("MSWrapper.Enums", 1, 0, "Enums");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
