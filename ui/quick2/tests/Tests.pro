TEMPLATE = app

TARGET = qmltests

QT += qml quick
CONFIG += c++11 warn_on qmltestcase

SOURCES += main.cpp

RESOURCES += qml.qrc \
    ../../images/images.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
