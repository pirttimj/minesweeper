/***********************************************************************
 * Copyright 2017 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file tst_selectionButton.qml
//! @brief Component SelectionButton tests.

import QtQuick 2.5
import QtTest 1.1
import "../.."

//! @class tst_selectionButton
//! @brief Component SelectionButton Unit Tests.
//!
//! After a mouse click a signal is emitted with parameters size and
//! level.
//!
//! Create a selection button with size Small and level Easy. Selection
//! signal checks:
//!
//! <ul>
//! <li>Signal parameter count must be 2 (size, level)</li>
//! <li>Signal parameters set to 'Small' and 'Easy'</li>
//! </ul>
//!
//! @addtogroup settingstests
//! @{
Item {
    height: 50;
    width: 100;

    //! @brief Game selection signal.
    //!
    //! @param size %Board size
    //! @param level Difficulty level
    signal selection(string size, string level);

    // Create Selection Button with size 'Small' and level 'Easy'
    SelectionButton {
        id: dut;
        anchors.fill: parent;
        size: "Small";
        level: "Easy";

        SignalSpy {
            id: spy;
            target: dut.parent;
            signalName: "selection";
        }
    }

    //! @brief Test Selection Button.
    TestCase {
        name: "SelectionButton";
        when: windowShown;

        function test_mouseClick() {
            var msg = "";

            verify(spy.count === 0);
            mouseClick(dut);
            verify(spy.count === 1);

            var expected = 2;
            var actual = spy.signalArguments[0].length;

            // Test selection signal parameter count.
            msg = "Incorrect parameter count. Expected " + expected
                + " parameters, got: " + actual;
            verify(actual === expected, msg);

            // Test parameter size
            expected = "Small";
            actual = spy.signalArguments[0][0];
            msg = "Incorrect parameter 'size'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);

            // Test parameter level
            expected = "Easy";
            actual = spy.signalArguments[0][1];
            msg = "Incorrect parameter 'level'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);
        }
    }
}
//! @}
