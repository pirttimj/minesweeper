/***********************************************************************
 * Copyright 2017 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file tst_selectionButtonGroup.qml
//! @brief Component SelectionButtonGroup tests.

import QtQuick 2.5
import QtTest 1.1
import "../.."

//! @class tst_selectionButtonGroup
//! @brief Component SelectionButtonGroup Unit Tests.
//!
//! After a mouse click on each Selection Button Group button the signal
//! settings is emitted with parameters size, level, rows, cols, and
//! mines.
//!
//! Expected results for each parameter:
//!
//! <table>
//! <tr><th>%Board Size</th><th>Difficulty Level</th><th>Rows</th>
//!     <th>Columns</th><th>Mines</th></tr>
//! <tr><td rowspan="3">Small</td><td>Easy</td><td rowspan="3">8</td>
//!     <td rowspan="3">8</td><td>10</td></tr>
//! <tr><td>Intermediate</td><td>12</td></tr>
//! <tr><td>Difficult</td><td>14</td></tr>
//! </table>
//!
//! @addtogroup settingstests
//! @{
Item {
    height: 450;
    width: 438;

    //! @brief Game settings signal.
    //!
    //! @param size %Board size
    //! @param level Game level
    //! @param rows Number of rows
    //! @param cols Number of columns
    //! @param mines Number of mines
    signal settings(
        string size, string level, int rows, int cols, int mines
    );

    SelectionButtonGroup {
        id: dut;
        anchors.fill: parent;
        label: "Small";

        SignalSpy {
            id: spySelection;
            target: dut;
            signalName: "selection";
        }

        SignalSpy {
            id: spySettings;
            target: dut.parent;
            signalName: "settings";
        }
    }

    //! @brief Selection Group Test.
    TestCase {
        name: "SelectionButtonGroup";
        when: windowShown;

        function init() {
            wait(0);

            verify(spySelection.valid);
            spySelection.clear();

            verify(spySettings.valid);
            spySettings.clear();
        }

        function test_settings(data) {
            verify(spySelection.count === 0);
            mouseClick(data.object);
            verify(spySelection.count === 1);

            // Test parameter count.
            var expected = 5;
            var actual = spySettings.signalArguments[0].length;
            var msg = "Incorrect parameter count. Expected " + expected
                + " parameters, got: " + actual;
            verify(actual === expected, msg);

            // Test parameter size
            expected = data.size;
            actual = spySettings.signalArguments[0][0];
            msg = "Incorrect parameter 'size'. Expected: "
                + expected + ", got: " + actual;
            compare(actual, expected, msg);

            // Test parameter level
            expected = data.level;
            actual = spySettings.signalArguments[0][1];
            msg = "Incorrect parameter 'level'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);

            // Test parameter rows
            expected = data.rows;
            actual = spySettings.signalArguments[0][2];
            msg = "Incorrect parameter 'rows'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);

            // Test parameter cols
            expected = data.cols;
            actual = spySettings.signalArguments[0][3];
            msg = "Incorrect parameter 'cols'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);

            // Test parameter mines
            expected = data.mines;
            actual = spySettings.signalArguments[0][4];
            msg = "Incorrect parameter 'mines'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);
        }

        function test_settings_data() {
            return [
                {
                    tag: "Small/Easy",
                    size: "Small",
                    level: "Easy",
                    rows: 8,
                    cols: 8,
                    mines: 10,
                    object: findChild(dut, "selectionEasy")
                },
                {
                    tag: "Small/Intermediate",
                    size: "Small",
                    level: "Intermediate",
                    rows: 8,
                    cols: 8,
                    mines: 12,
                    object: findChild(dut, "selectionIntermediate")
                },
                {
                    tag: "Small/Difficult",
                    size: "Small",
                    level: "Difficult",
                    rows: 8,
                    cols: 8,
                    mines: 14,
                    object: findChild(dut, "selectionDifficult")
                }
            ];
        }
    }
}
//! @}
