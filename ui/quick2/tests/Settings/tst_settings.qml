/***********************************************************************
 * Copyright 2017 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file tst_settings.qml
//! @brief Component Settings tests.

import QtQuick 2.5
import QtTest 1.1
import "../.."

//! @class tst_settings
//! @brief Component Settings Unit Tests.
//!
//! After a mouse click a signal settings is emitted with parameters
//! size, level, rows, cols, and mines.
//!
//! Expected results for each parameter:
//!
//! <table>
//! <tr><th>%Board Size</th><th>Difficulty Level</th><th>Rows</th>
//!     <th>Columns</th><th>Mines</th></tr>
//! <tr><td rowspan="3">Small</td><td>Easy</td><td rowspan="3">8</td>
//!     <td rowspan="3">8</td><td>10</td></tr>
//! <tr><td>Intermediate</td><td>12</td></tr>
//! <tr><td>Difficult</td><td>14</td></tr>
//! <tr><td rowspan="3">Medium</td><td>Easy</td><td rowspan="3">16</td>
//!     <td rowspan="3">16</td><td>40</td></tr>
//! <tr><td>Intermediate</td><td>48</td></tr>
//! <tr><td>Difficult</td><td>56</td></tr>
//! <tr><td rowspan="3">Large</td><td>Easy</td><td rowspan="3">16</td>
//!     <td rowspan="3">30</td><td>75</td></tr>
//! <tr><td>Intermediate</td><td>90</td></tr>
//! <tr><td>Difficult</td><td>105</td></tr>
//! <tr><td rowspan="2">Custom</td><td rowspan="2">Custom</td>
//!     <td>2</td><td>2</td><td>1</td>
//! <tr><td>99</td><td>99</td>
//!     <td>9800</td>
//! </tr>
//! </table>
//!
//! @addtogroup settingstests
//! @{
Item {
    height: 450;
    width: 438;

    Settings {
        id: dut;
        anchors.fill: parent;

        SignalSpy {
            id: spy;
            target: dut;
            signalName: "settings";
        }
    }

    //! @brief Selection Group Test.
    TestCase {
        name: "Settings";
        when: windowShown;

        function init() {
            wait(0);
            verify(spy.valid);
            spy.clear();
        }

        function test_settings(data) {
            verify(spy.count === 0);
            mouseClick(data.object);
            verify(spy.count === 1);

            // Test settings signal parameter count.
            var expected = 5;
            var actual = spy.signalArguments[0].length;
            var msg = "Incorrect parameter count. Expected " + expected
                + " parameters, got: " + actual;
            verify(actual === expected, msg);

            // Test parameter size
            expected = data.size;
            actual = spy.signalArguments[0][0];
            msg = "Incorrect parameter 'size'. Expected: "
                + expected + ", got: " + actual;
            compare(actual, expected, msg);

            // Test parameter level
            expected = data.level;
            actual = spy.signalArguments[0][1];
            msg = "Incorrect parameter 'level'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);

            // Test parameter rows
            expected = data.rows;
            actual = spy.signalArguments[0][2];
            msg = "Incorrect parameter 'rows'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);

            // Test parameter cols
            expected = data.cols;
            actual = spy.signalArguments[0][3];
            msg = "Incorrect parameter 'cols'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);

            // Test parameter mines
            expected = data.mines;
            actual = spy.signalArguments[0][4];
            msg = "Incorrect parameter 'mines'. Expected: " + expected
                + ", got: " + actual;
            compare(actual, expected, msg);
        }

        function test_settings_data() {
            return [
                {
                    tag: "Small/Easy",
                    size: "Small",
                    level: "Easy",
                    rows: 8,
                    cols: 8,
                    mines: 10,
                    object: findChild(findChild(dut, "selectionSmall"),
                                      "selectionEasy")
                },
                {
                    tag: "Small/Intermediate",
                    size: "Small",
                    level: "Intermediate",
                    rows: 8,
                    cols: 8,
                    mines: 12,
                    object: findChild(findChild(dut, "selectionSmall"),
                                      "selectionIntermediate")
                },
                {
                    tag: "Small/Difficult",
                    size: "Small",
                    level: "Difficult",
                    rows: 8,
                    cols: 8,
                    mines: 14,
                    object: findChild(findChild(dut, "selectionSmall"),
                                      "selectionDifficult")
                },
                {
                    tag: "Medium/Easy",
                    size: "Medium",
                    level: "Easy",
                    rows: 16,
                    cols: 16,
                    mines: 40,
                    object: findChild(findChild(dut, "selectionMedium"),
                                      "selectionEasy")
                },
                {
                    tag: "Medium/Intermediate",
                    size: "Medium",
                    level: "Intermediate",
                    rows: 16,
                    cols: 16,
                    mines: 48,
                    object: findChild(findChild(dut, "selectionMedium"),
                                      "selectionIntermediate")
                },
                {
                    tag: "Medium/Difficult",
                    size: "Medium",
                    level: "Difficult",
                    rows: 16,
                    cols: 16,
                    mines: 56,
                    object: findChild(findChild(dut, "selectionMedium"),
                                      "selectionDifficult")
                },
                {
                    tag: "Large/Easy",
                    size: "Large",
                    level: "Easy",
                    rows: 16,
                    cols: 30,
                    mines: 75,
                    object: findChild(findChild(dut, "selectionLarge"),
                                      "selectionEasy")
                },
                {
                    tag: "Large/Intermediate",
                    size: "Large",
                    level: "Intermediate",
                    rows: 16,
                    cols: 30,
                    mines: 90,
                    object: findChild(findChild(dut, "selectionLarge"),
                                      "selectionIntermediate")
                },
                {
                    tag: "Large/Difficult",
                    size: "Large",
                    level: "Difficult",
                    rows: 16,
                    cols: 30,
                    mines: 105,
                    object: findChild(findChild(dut, "selectionLarge"),
                                      "selectionDifficult")
                }
            ];
        }
    }
}
//! @}
