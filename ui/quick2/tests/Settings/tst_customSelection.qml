/***********************************************************************
 * Copyright 2017 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file tst_customSelection.qml
//! @brief Component CustomSelection tests.

import QtQuick 2.5
import QtTest 1.1
import "../.."

//! @class tst_customSelection
//! @brief Component CustomSelection Unit Tests.
//!
//! Checks:
//!
//! <ul>
//! <li>Signal parameter count must be 5</li>
//! <li>Signal parameters size and level are set to 'Custom' and
//!     'Custom'</li>
//! <li>SpinBox Rows accepts values between 2-99</li>
//! <li>SpinBox Columns accepts values between 2-99</li>
//! <li>SpinBox Mines accepts values between 1-rows&times;cols</li>
//! </ul>
//!
//! @addtogroup settingstests
//! @{
Item {
    height: 400;
    width: 400;

    //! @brief Signal settings
    //!
    //! @param size %Board size
    //! @param level Game level
    //! @param rows Number of rows
    //! @param cols Number of columns
    //! @param mines Number of mines
    signal settings(
        string size, string level, int rows, int cols, int mines
    );

    CustomSelection {
        id: dut;
        anchors.fill: parent;
    }

    SignalSpy {
        id: spy;
        target: dut.parent;
        signalName: "settings";
    }

    TestCase {
        name: "CustomSelection";
        when: windowShown;

        function init() {
            wait(0);
            verify(spy.valid);
            spy.clear();

            // Set board default values
            var rows = findChild(dut, "spinBoxRows");
            var cols = findChild(dut, "spinBoxColumns");
            var mines = findChild(dut, "spinBoxMines");

            rows.value = 2;
            cols.value = 2;
            mines.value = 1;
        }

        // Test SpinBoxes
        // Test 1: Rows spinbox tests
        // Test 2: Columns spinbox tests
        // Test 3: Mines spinbox tests
        // Test 4: Mines spinbox tests
        function test_spinbox_minmax(data) {
            var rows = findChild(dut, "spinBoxRows");
            var cols = findChild(dut, "spinBoxColumns");
            var mines = findChild(dut, "spinBoxMines");

            // Test SpinBox rows
            spinbox_minmax_tests(rows, data.rows);

            // Test SpinBox columns
            spinbox_minmax_tests(cols, data.cols);

            // Test SpinBox mines
            spinbox_mine_tests(rows, cols, mines, data);

            // Test SpinBox mines
            spinbox_mines_enabled_tests(rows, cols, mines);
        }

        // Test decreasing/increasing spinbox value below/above
        // minimum/maximum values
        // Test 1:
        // 1. Check that spinbox value matches default value initially
        //
        // Test 2:
        // 1. Set spinbox value to minimum
        // 2. Click on spinbox down indicator
        // 3. Check that spinbox value is minimum (no change)
        //
        // Test 3:
        // 1. Set spinbox value to maximum
        // 2. Click on spinbox up indicator
        // 3. Check that spinbox value is maximum (no change)
        function spinbox_minmax_tests(object, data) {
            var msg = "Incorrect " + object.objectName
                + " default value.";

            // Test spinbox default value
            compare(object.value, data.default, msg);

            // Test decreasing spinbox minimum value
            msg = object.objectName + " failed when decreasing minimum "
                + "value.";
            object.value = data.min;
            mouseClick(object.down.indicator);
            compare(object.value, data.min, msg);

            // Test increasing spinbox maximum value
            msg = object.objectName + " failed when increasing maximum "
                + "value.";
            object.value = data.max;
            mouseClick(object.up.indicator);
            compare(object.value, data.max, msg);
        }

        // Test decreasing spinbox mines minimum value below minimum and
        // maximum value above maximum. Test boards: 2x2, 1 mine
        // (minimum values), 99x99, 9800 mines (maximum values), and
        // with random values.
        // Test 1:
        // Board 2x2, 1 mine
        // 1. Set mines spinbox value to minimum
        // 2. Click on spinbox down indicator
        // 3. Check that spinbox value is minimum (no change)
        //
        // Test 2:
        // Board 99x99, 9800 mines
        // 1. Set mines spinbox value to maximum
        // 2. Click on spinbox up indicator
        // 3. Check that spinbox value is maximum (no change)
        //
        // Test 3:
        // Random board
        // 1. Set rows spinbox to random value (2-99)
        // 2. Set columns spinbox to random value (2-99)
        // 3. Set mines spinbox to maximum value (rows x cols - 1)
        // 4. Click on spinbox up indicator
        // 5. Check that spinbox value is maximum (no change)
        function spinbox_mine_tests(rows, cols, mines, data) {
            var msg = "";
            // Board setup: 2x2, 1 mine
            rows.value = data.rows.min;
            cols.value = data.cols.min;

            // Test decreasing SpinBox mines default min value
            msg = mines.objectName + " failed when decreasing minimum "
                + "value.";
            // Set mines minimum count
            mines.value = data.mines.min;
            // Try decreasing mines count
            mouseClick(mines.down.indicator);
            // Check mines count
            compare(mines.value, data.mines.min, msg);

            // Test increasing SpinBox mines default max value
            msg = mines.objectName + " failed when increasing maximum "
                + "value.";
            // Board setup: 99x99, 9800 mine
            rows.value = data.rows.max;
            cols.value = data.cols.max;
            mines.value = data.mines.max;
            // Try increase mines count
            mouseClick(mines.up.indicator);
            // Check mines count
            compare(mines.value, data.mines.max, msg);

            // Test with random values
            rows.value = Math.floor(Math.random() * (99 - 2 + 1)) + 2;
            cols.value = Math.floor(Math.random() * (99 - 2 + 1)) + 2;
            var minesMaxCount = (rows.value * cols.value) - 1;
            mines.value = minesMaxCount;
            msg += " Board: " + rows.value + "x" + cols.value + ".";

            // Try increase mines count
            mouseClick(mines.up.indicator);
            // Check mines count
            compare(mines.value, minesMaxCount, msg);
        }

        // Test data for test_spinbox_minmax function
        function test_spinbox_minmax_data() {
            return [
                {
                    tag: "Min/max values",
                    rows: {
                        "min": 2,
                        "max": 99,
                        "default": 2
                    },
                    cols: {
                        "min": 2,
                        "max": 99,
                        "default": 2
                    },
                    mines: {
                        "min": 1,
                        "max": 9800,
                        "default": 1
                    },
                }
            ];
        }

        // Test mines spinbox up indicator enabled state
        // Test 1:
        // 1. Set board 2x2 and 3 mines
        // 2. Mines spinbox up indicator should be disabled
        // 3. Increase rows count to 3
        // 4. Mines spinbox up indicator should be enabled
        //
        // Test 2:
        // 1. Set board 2x2 and 3 mines
        // 2. Mines spinbox up indicator should be disabled
        // 3. Increase columns count to 3
        // 4. Mines spinbox up indicator should be enabled
        function spinbox_mines_enabled_tests(rows, cols, mines) {
            var msg = "";

            // Test 1
            // Set board
            rows.value = 2;
            cols.value = 2;
            mines.value = 3;

            msg = mines.objectName + ".up.indicator: wrong enabled "
                + "state before increasing row count.";
            // Check mines up indicator enabled state
            compare(mines.up.indicator.enabled, false, msg);

            // Increase rows count to 3
            mouseClick(rows.up.indicator);

            msg = mines.objectName + ".up.indicator: wrong enabled "
                + "state after increasing row count.";
            // Check mines up indicator enabled state
            compare(mines.up.indicator.enabled, true, msg);

            // Test 2
            // Set board
            rows.value = 2;
            cols.value = 2;
            mines.value = 3;

            msg = mines.objectName + ".up.indicator: wrong enabled "
                + "state before increasing column count.";
            // Check mines up indicator enabled state
            compare(mines.up.indicator.enabled, false, msg);

            // Increase columns count to 3
            mouseClick(cols.up.indicator);

            msg = mines.objectName + ".up.indicator: wrong enabled "
                + "state after increasing column count.";
            // Check mines up indicator enabled state
            compare(mines.up.indicator.enabled, true, msg);
        }

        // Test signal settings on start button click
        // Test 1:
        // Board 2x2, 1 mine
        // settings: custom, custom, 2, 2, 1
        //
        // Test 2:
        // Board 99x99, 9800 mines
        // settings: custom, custom, 99, 99, 9800
        //
        // Test 3:
        // Random board, random mines count
        function test_customSelection(data) {
            var msg = "Signal settings failed.";
            var start = findChild(dut, "button");
            var rows = findChild(dut, "spinBoxRows");
            var cols = findChild(dut, "spinBoxColumns");
            var mines = findChild(dut, "spinBoxMines");

            // Setup board
            rows.value = data.rows;
            cols.value = data.cols;
            mines.value = data.mines;

            // Click on start
            mouseClick(start);

            // Check settings signal parameters
            // Parameter size
            compare(spy.signalArguments[0][0], "Custom",
                    msg + " Parameter: size");

            // Parameter level
            compare(spy.signalArguments[0][1], "Custom",
                    msg + " Parameter: level");

            // Parameter rows
            compare(spy.signalArguments[0][2], data.rows,
                    msg + " Parameter: rows");

            // Parameter cols
            compare(spy.signalArguments[0][3], data.cols,
                    msg + " Parameter: cols");

            // Parameter mines
            compare(spy.signalArguments[0][4], data.mines,
                    msg + " Parameter: mines");
        }

        // Test data for test_customSelection function
        function test_customSelection_data() {
            var rows = Math.floor(Math.random() * (99 - 2 + 1)) + 2;
            var cols = Math.floor(Math.random() * (99 - 2 + 1)) + 2;
            var mines = Math.floor(
                Math.random() * ((rows*cols-1) - 2 + 1)) + 2;

            return [
                {
                    tag: "2x2/1",
                    rows: 2,
                    cols: 2,
                    mines: 1
                },
                {
                    tag: "99x99/9800",
                    rows: 99,
                    cols: 99,
                    mines: 9800
                },
                {
                    tag: "Random",
                    rows: rows,
                    cols: cols,
                    mines: mines
                }
            ];
        }
    }
}
//! @}
