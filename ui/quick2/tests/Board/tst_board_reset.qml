/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file tst_board_reset.qml
//! @brief Component Board reset tests.

import QtQuick 2.5
import QtTest 1.0
import "../.."

//! @class tst_board_reset
//! @brief Component Board Reset Unit Tests.
//!
//! All tiles set to state UNKNOWN after board reset.
//!
//! @addtogroup boardtests
//! @{
Item {
    width: 67;
    height: 67;

    //! @brief Board Reset Test.
    Board {
        id: dut;
        anchors.fill: parent;
        rows: 2;
        cols: 2;
    }

    TestCase {
        name: "Board#Reset";
        when: windowShown;

        function test_reset() {
            var msg = name + ": Function reset() failed to reset board "
                    + "tile at index ";

            // Check state in each tile on board
            for (var i=0; i<dut.tiles.count; i++) {
                var actual = dut.tiles.itemAt(i).state;
                compare(actual, "UNKNOWN", msg + i);
            }
        }
    }
}
//! @}
