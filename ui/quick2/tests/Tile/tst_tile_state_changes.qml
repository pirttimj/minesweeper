/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file tst_tile_state_changes.qml
//! @brief Component Tile state change tests.

import QtQuick 2.5
import QtTest 1.0
import "../.."

//! @class tst_tile_state_changes
//! @brief Component Tile State Change Unit Tests.
//!
//! Tile state change tests.
//!
//! <table>
//! <tr>
//! <th>Initial State</th><th>Mouse Action</th><th>Next State</th>
//! </tr>
//! <tr><td>UNKNOWN</td><td>Qt.LeftButton</td><td>REVEALED</td></tr>
//! <tr><td>UNKNOWN</td><td>Qt.RightButton</td><td>FLAGGED</td></tr>
//! <tr><td>FLAGGED</td><td>Qt.LeftButton</td><td>FLAGGED</td></tr>
//! <tr><td>FLAGGED</td><td>Qt.RightButton</td><td>UNKNOWN</td></tr>
//! <tr><td>REVEALED</td><td>Qt.LeftButton</td><td>REVEALED</td></tr>
//! <tr><td>REVEALED</td><td>Qt.RightButton</td><td>REVEALED</td></tr>
//! </table>
//!
//! @addtogroup tiletests
//! @{
Item {
    width: 32;
    height: 32;

    Tile {
        id: dut;
        anchors.fill: parent;
    }

    TestCase {
        name: "Tile#State Changes";
        when: windowShown;

        function test_state_changes(data) {
            var position =
                dut.mapFromItem(dut, dut.width/2, dut.height/2);
            var msg = name + ": State change failure.";

            // Set initial state
            dut.state = data.initial;

            // Action
            mouseClick(dut, position.x, position.y, data.action, 0, 0);

            // Verify
            var actual = dut.state;
            compare(actual, data.expected, msg);
        }

        function test_state_changes_data() {
            return [
                {
                    tag: "UNKNOWN + Left Click",
                    initial: "UNKNOWN",
                    action: Qt.LeftButton,
                    expected: "REVEALED"
                },
                {
                    tag: "UNKNOWN + Right Click",
                    initial: "UNKNOWN",
                    action: Qt.RightButton,
                    expected: "FLAGGED"
                },
                {
                    tag: "FLAGGED + Left Click",
                    initial: "FLAGGED",
                    action: Qt.LeftButton,
                    expected: "FLAGGED"
                },
                {
                    tag: "FLAGGED + Right Click",
                    initial: "FLAGGED",
                    action: Qt.RightButton,
                    expected: "UNKNOWN"
                },
                {
                    tag: "REVEALED + Left Click",
                    initial: "REVEALED",
                    action: Qt.LeftButton,
                    expected: "REVEALED"
                },
                {
                    tag: "REVEALED + Right Click",
                    initial: "REVEALED",
                    action: Qt.RightButton,
                    expected: "REVEALED"
                }
            ];
        }
    }
}
//! @}
