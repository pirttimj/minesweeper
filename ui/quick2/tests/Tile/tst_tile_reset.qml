/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file tst_tile_reset.qml
//! @brief Component Tile reset tests.

import QtQuick 2.5
import QtTest 1.0
import "../.."

//! @class tst_tile_reset
//! @brief Component Tile Reset Unit Tests.
//!
//! Tile reset tests.
//!
//! <table>
//! <tr><th>Initial state</th><th>Next State</th></tr>
//! <tr><td>UNKNOWN</td><td>UNKNOWN</td></tr>
//! <tr><td>FLAGGED</td><td>UNKNOWN</td></tr>
//! <tr><td>REVEALED</td><td>UNKNOWN</td></tr>
//! </table>
//!
//! @addtogroup tiletests
//! @{
Item {
    width: 32;
    height: 32;

    Tile {
        id: dut;
        anchors.fill: parent;
    }

    //! @brief Tile Reset Test.
    //!
    //! 
    TestCase {
        name: "Tile#Reset";
        when: windowShown;

        function test_reset() {
            // Reset in state UNKNOWN
            dut.state = "UNKNOWN";
            dut.reset();

            reset_tests(dut);

            // Reset in state FLAGGED
            dut.state = "FLAGGED";
            dut.reset();

            reset_tests(dut);

            // Reset in state REVEALED
            dut.state = "REVEALED";
            dut.reset();

            reset_tests(dut);
        }

        function reset_tests(dut) {
            var actual = {
                state: dut.state,
                enabled: dut.tileMouseArea.enabled,
                color: dut.tileBackground.color,
                image: dut.tileImage.source
            };
            var expected = {
                state: "UNKNOWN",
                enabled: true,
                color: "#d3d3d3", // lightgray
                image: "qrc:/images/Empty"
            };
            var msg = name + ": Function reset() failed from state "
                    + actual.state + " ";

            // Run reset tests
            compare(actual.state, expected.state,
                msg + "(tile.state).");

            compare(actual.enabled, expected.enabled,
                msg + "(mousearea.enabled).");

            compare(actual.image, expected.image,
                msg + "(image.source).");

            compare(actual.color, expected.color,
                msg + "(tile.background.color).");
        }
    }
}
//! @}
