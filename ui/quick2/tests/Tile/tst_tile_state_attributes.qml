/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file tst_tile_state_attributes.qml
//! @brief Component Tile state attributes tests.

import QtQuick 2.5
import QtTest 1.0
import "../.."

//! @class tst_tile_state_attributes
//! @brief Component Tile State Attributes Unit Tests.
//!
//! Tile state attributes tests.
//!
//! <table>
//! <tr><th>State</th><th>Enabled</th><th>Color</th><th>Image</th></tr>
//! <tr><td>UNKNOWN</td><td>true</td><td>\#d3d3d3 (light gray)</td><td>Empty</td></tr>
//! <tr><td>FLAGGED</td><td>true</td><td>\#d3d3d3 (light gray)</td><td>Flag</td></tr>
//! <tr><td>REVEALED</td><td>false</td><td>\#a9a9a9 (dark gray)</td><td>Empty</td></tr>
//! </table>
//!
//! @addtogroup tiletests
//! @{
Item {
    width: 32;
    height: 32;

    Tile {
        id: dut;
        anchors.fill: parent;
    }

    TestCase {
        name: "Tile#State Attributes";
        when: windowShown;

        function test_state_attributes(data) {
            // Set state
            dut.state = data.state;

            var actual = {
                state: dut.state,
                enabled: dut.tileMouseArea.enabled,
                color: dut.tileBackground.color,
                image: dut.tileImage.source
            };

            var msg = name + ": State attributes failure ("
                    + actual.state + ": ";

            // Run state attributes tests
            compare(actual.enabled, data.enabled,
                msg + "mousearea.enabled).");

            compare(actual.image, data.image,
                msg + "image.source).");

            compare(actual.color, data.color,
                msg + "tile.background.color).");
        }

        function test_state_attributes_data() {
            return [
                {
                    tag: "UNKNOWN",
                    state: "UNKNOWN",
                    enabled: true,
                    color: "#d3d3d3", // lightgray
                    image: "qrc:/images/Empty"
                },
                {
                    tag: "FLAGGED",
                    state: "FLAGGED",
                    enabled: true,
                    color: "#d3d3d3", // lightgray
                    image: "qrc:/images/Flag"
                },
                {
                    tag: "REVEALED",
                    state: "REVEALED",
                    enabled: false,
                    color: "#a9a9a9", // dark gray
                    image: "qrc:/images/Empty"
                }
            ];
        }
    }
}
//! @}
