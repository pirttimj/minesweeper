/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file SelectionButtonGroup.qml
//! @brief Component SelectionButtonGroup functionality.

import QtQuick 2.4

//! @class SelectionButtonGroup
//! @brief SelectionButtonGroup functionality
//! @addtogroup settings
//! @{
SelectionButtonGroupForm {
    onSelection: {
        // Default values for each selection button group
        var defaults = {
            "Small": {
                "rows": 8,
                "cols": 8,
                "mines": {
                    "Easy": 10,
                    "Intermediate": 12,
                    "Difficult": 14
                }
            },
            "Medium": {
                "rows": 16,
                "cols": 16,
                "mines": {
                    "Easy": 40,
                    "Intermediate": 48,
                    "Difficult": 56
                }
            },
            "Large": {
                "rows": 16,
                "cols": 30,
                "mines": {
                    "Easy": 75,
                    "Intermediate": 90,
                    "Difficult": 105
                }
            }
        };

        settings(size, level, defaults[size].rows, defaults[size].cols,
                 defaults[size].mines[level]);
    }
}
//! @}
