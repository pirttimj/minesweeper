/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file CustomSelectionForm.ui.qml
//! @brief Component CustomSelection UI form.

import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

//! @class CustomSelectionForm
//! @brief CustomSelection UI form.
//!
//! Custom selection form. Allows customizing rows, columns, and mines
//! counts.
//!
//! @addtogroup settings
//! @{
Item {
    Layout.fillHeight: true;
    Layout.fillWidth: true;

    //! @brief Custom selection button mouse area
    property alias mouseArea: mouseArea
    //! @brief Rows spin box
    property alias spinBoxRows: spinBoxRows
    //! @brief Columns spin box
    property alias spinBoxColumns: spinBoxColumns
    //! @brief Mines spin box
    property alias spinBoxMines: spinBoxMines

    Rectangle {
        id: background;
        color: "transparent";
        anchors.fill: parent;
    }

    ColumnLayout {
        id: columnLayout;
        anchors.rightMargin: 10;
        anchors.leftMargin: 10;
        anchors.bottomMargin: 10;
        anchors.topMargin: 10;
        anchors.fill: parent;

        Text {
            id: label;
            text: qsTr("Custom");
            Layout.fillWidth: true;
            verticalAlignment: Text.AlignVCenter;
            horizontalAlignment: Text.AlignHCenter;
            font.pixelSize: 12;
        }

        GridLayout {
            id: gridLayout;
            Layout.fillHeight: false;
            Layout.fillWidth: false;
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter;
            columns: 2;

            Text {
                id: rows;
                text: qsTr("Rows");
                font.pixelSize: 12;
            }

            SpinBox {
                id: spinBoxRows;
                objectName: "spinBoxRows";
                from: 2;
                value: 2;
            }

            Text {
                id: cols;
                text: qsTr("Columns");
                font.pixelSize: 12;
            }

            SpinBox {
                id: spinBoxColumns;
                objectName: "spinBoxColumns";
                from: 2;
                value: 2;
            }

            Text {
                id: mines;
                text: qsTr("Mines");
                font.pixelSize: 12;
            }

            SpinBox {
                id: spinBoxMines;
                objectName: "spinBoxMines";
                from: 1;
                to: spinBoxRows.value*spinBoxColumns.value-1;
                value: 1;
            }

            Rectangle {
                id: rectangle;
                objectName: "button";
                color: "lightgray";
                Layout.fillWidth: true;
                Layout.columnSpan: 2;
                height: spinBoxColumns.height;

                Text {
                    id: text1;
                    text: qsTr("Start");
                    verticalAlignment: Text.AlignVCenter;
                    horizontalAlignment: Text.AlignHCenter;
                    anchors.fill: parent;
                    font.pixelSize: 12;
                }

                MouseArea {
                    id: mouseArea;
                    anchors.fill: parent;
                }
            }
        }
    }
}
//! @}
