/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file NewGameButtonForm.ui.qml
//! @brief Component NewGameButton UI Form.

import QtQuick 2.4
import QtQuick.Layouts 1.3

//! @class NewGameButtonForm
//! @brief NewGameButton UI form.
//!
//! @addtogroup newGameButton
//! @{
Item {
    height: 40
    width: 40
    state: "SMILE"

    //! @brief New game button mouse area
    property alias mouseArea: mouseArea

    Rectangle {
        id: background
        color: "lightgray"
        height: parent.height
        width: parent.width

        Image {
            id: face
            width: 32
            height: 32
            anchors.centerIn: parent
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
        }
    }

    states: [
        State {
            name: "SMILE"

            PropertyChanges {
                target: face
                source: "qrc:/images/Smile"
            }
        },
        State {
            name: "SCARED"

            PropertyChanges {
                target: face
                source: "qrc:/images/Scared"
            }
        },
        State {
            name: "WINNER"

            PropertyChanges {
                target: face
                source: "qrc:/images/Winner"
            }
        },
        State {
            name: "LOSER"

            PropertyChanges {
                target: face
                source: "qrc:/images/Loser"
            }
        }
    ]
}
//! @}
