/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file SelectionButtonForm.ui.qml
//! @brief Component SelectionButton UI form.

import QtQuick 2.4
import QtQuick.Layouts 1.3

//! @class SelectionButtonForm
//! @brief SelectionButton UI form.
//! @addtogroup settings
//! @{
Item {
    Layout.fillHeight: true;
    Layout.fillWidth: true;

    //! @brief Selection button group title
    property string size: "Undefined";
    //! @brief Selection button text string
    property alias level: label.text;
    //! @brief Selection button mouse area
    property alias mouseArea: mouseArea;

    Rectangle {
        id: background;
        color: "lightgray";
        anchors.fill: parent;
    }

    Text {
        id: label;
        text: qsTr("Level");
        verticalAlignment: Text.AlignVCenter;
        horizontalAlignment: Text.AlignHCenter;
        anchors.fill: parent;
        font.pixelSize: 12;
    }

    MouseArea {
        id: mouseArea;
        anchors.fill: parent;
    }
}
//! @}
