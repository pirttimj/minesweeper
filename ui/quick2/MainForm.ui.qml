/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file MainForm.ui.qml
//! @brief Minesweeper main window UI form.

import QtQuick 2.5
import QtQuick.Layouts 1.1

//! @class MainForm
//! @brief Minesweeper main window UI form.
//! @addtogroup ui
//! @{
Item {
    id: form;

    //! type:Board Game board
    property alias board: board

    ColumnLayout {
        anchors.fill: parent
        spacing: 0;

        StatusBar {
            id: statusBar
            height: 48
            Layout.fillWidth: true
        }

        Board {
            id: board
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
}
//! @}
