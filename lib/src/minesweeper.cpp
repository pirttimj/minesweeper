/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file minesweeper.cpp

#include "minesweeper.h"

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <cassert>

MS::Minesweeper::Minesweeper() : rows_(0), cols_(0), mines_({}),
    states_({})
{
    srand((unsigned int)time(NULL));
}

MS::Minesweeper::~Minesweeper()
{

}

void MS::Minesweeper::setup(
    unsigned int rows,
    unsigned int cols,
    unsigned int mines
    )
{
    // Mine count must be within range 0 to rows*cols
    assert(mines <= rows*cols);

    // Setup board
    rows_ = rows;
    cols_ = cols;

    // Resize states vector if necessary
    if (states_.size() != rows*cols)
    {
        states_.resize(rows*cols);
    }

    // Reset states vector
    std::fill(states_.begin(), states_.end(), UNKNOWN);
}

void MS::Minesweeper::setMines(unsigned int mines, unsigned int first)
{
    // Mine count must be within range 0 to rows*cols
    assert(mines <= rows_*cols_);

    unsigned int max = rows_*cols_;
    unsigned int i = 0;

    // Remove old values
    mines_.clear();

    while (i < mines)
    {
        // Get a new location between range 0 to max
        unsigned int location = ((unsigned int)rand() % max);

        // Location must be within range 0 to rows*cols
        assert(location <= rows_*cols_);

        // No mines at first selected square
        if (location == first)
        {
            continue;
        }

        // Check if new location is duplicate
        if (std::find(mines_.begin(), mines_.end(), location)
            == mines_.end())
        {
            // Not a duplicate
            mines_.push_back(location);
            ++i;
        }
    }

    // Sort mines array in ascending order
    std::sort(mines_.begin(), mines_.end());
}

bool MS::Minesweeper::isMined(unsigned int location) const
{
    // Location must be within range 0 to rows*cols-1
    assert(location <= rows_*cols_-1);

    return (std::find(std::begin(mines_), std::end(mines_), location)
        != mines_.end());
}

MS::Minesweeper::States MS::Minesweeper::getState(
    unsigned int location
    ) const
{
    // Location must be within range 0 to rows*cols-1
    assert(location <= rows_*cols_-1);

    return states_.at(location);
}

MS::Minesweeper::States MS::Minesweeper::toggleFlag(
    unsigned int location
    )
{
    // Location must be within range 0 to rows*cols-1
    assert(location <= rows_*cols_-1);

    // Get current state
    States state = states_.at(location);

    if (state != REVEALED)
    {
        // Toggle flag state
        states_.at(location) = (state == UNKNOWN) ? FLAGGED : UNKNOWN;
    }

    return states_.at(location);
}

std::vector<unsigned int> MS::Minesweeper::getFlags() const
{
    std::vector<unsigned int> flags;

    for (auto it = states_.begin(); it != states_.end(); ++it)
    {
        unsigned int index = (unsigned int)(it - states_.begin());

        if (states_.at(index) == FLAGGED)
        {
            flags.push_back(index);
        }
    }

    return flags;
}

std::vector<unsigned int> MS::Minesweeper::getAdjacentSquares(
    unsigned int location
    ) const
{
    // Location must be within range 0 to rows*cols-1
    assert(location <= rows_*cols_-1);

    std::vector<unsigned int> adjacentSquares;

    // Main directions
    bool N = (location >= cols_);
    bool E = ((location % cols_) != cols_ - 1);
    bool S = (location < (cols_ * (rows_ - 1)));
    bool W = ((location % cols_) > 0);

    // Create a list of adjacent square indexes in increasing index
    // order (i.e. NW, N, NE, W, E, SW, S, SE)
    if (N && W)
    {
        adjacentSquares.push_back(location - cols_ - 1);
    }

    if (N)
    {
        adjacentSquares.push_back(location - cols_);
    }

    if (N && E)
    {
        adjacentSquares.push_back(location - cols_ + 1);
    }

    if (W)
    {
        adjacentSquares.push_back(location - 1);
    }

    if (E)
    {
        adjacentSquares.push_back(location + 1);
    }

    if (S && W)
    {
        adjacentSquares.push_back(location + cols_ - 1);
    }

    if (S)
    {
        adjacentSquares.push_back(location + cols_);
    }

    if (S && E)
    {
        adjacentSquares.push_back(location + cols_ + 1);
    }

    return adjacentSquares;
}

unsigned int MS::Minesweeper::getAdjacentMineCount(
    unsigned int location
    ) const
{
    // Location must be within range 0 to rows*cols-1
    assert(location <= rows_*cols_-1);

    unsigned int mineCount = 0;

    // Get adjacent squares
    std::vector<unsigned int> adjacentSquares
        = getAdjacentSquares(location);

    // Get adjacent mine count
    for_each(adjacentSquares.begin(), adjacentSquares.end(),
        [&](unsigned int i)
        {
            if (isMined(i))
            {
                mineCount++;
            }
        }
    );

    return mineCount;
}

std::vector<unsigned int> MS::Minesweeper::select(
    unsigned int location
    )
{
    // Location must be within range 0 to rows*cols-1
    assert(location <= rows_*cols_-1);

    std::vector<unsigned int> selection = {};

    if (getState(location) != UNKNOWN)
    {
        // Square state is either FLAGGED or REVEALED, return nothing
        return selection;
    }

    selection = {location};

    if (isMined(location))
    {
        // Square contains a mine, return just current location
        states_.at(location) = REVEALED;
        return selection;
    }

    if (getAdjacentMineCount(location) != 0)
    {
        // Mines in adjacent squares, return just current location
        states_.at(location) = REVEALED;
        return selection;
    }

    getSurroundingSquares(location, selection);

    return selection;
}

bool MS::Minesweeper::isFinished() const
{
    return ((unsigned int)std::count(
        states_.begin(), states_.end(), REVEALED)
        == rows_*cols_ - mines_.size()
    );
}

////////////////////////////////////////////////////////////////////////
// Helper methods
////////////////////////////////////////////////////////////////////////

double MS::Minesweeper::getDistance(
    unsigned int from,
    unsigned int to
    ) const
{
    // Locations must be within range 0 to rows*cols-1
    assert(from <= rows_*cols_-1);
    assert(to <= rows_*cols_-1);

    int rdelta = abs((int)from/(int)cols_ - (int)to/(int)cols_);
    int cdelta = abs((int)from%(int)cols_ - (int)to%(int)cols_);

    return sqrt(pow(rdelta, 2) + pow(cdelta, 2));
}

void MS::Minesweeper::getSurroundingSquares(
    unsigned int location,
    std::vector<unsigned int>& squares
    )
{
    // Location must be within range 0 to rows*cols-1
    assert(location <= rows_*cols_-1);
    // Vector 'squares' size must be between 0 to rows*cols
    assert(squares.size() <= rows_*cols_);
    // Each value in vector 'squares' must be within range 0 to
    // rows*cols-1
    for (auto i : squares)
    {
        assert(i <= (rows_*cols_-1));
    }

    if (getState(location) == UNKNOWN)
    {
        states_.at(location) = REVEALED;

        if (getAdjacentMineCount(location) == 0)
        {
            // No adjacent mines, get all adjacent UNKNOWN squares
            std::vector<unsigned int> adjacentSquares
                = getAdjacentSquares(location);

            for (auto adjacent : adjacentSquares)
            {
                if (getState(adjacent) == UNKNOWN)
                {
                    squares.push_back(adjacent);
                }

                getSurroundingSquares(adjacent, squares);
            }
        }
    }
}
