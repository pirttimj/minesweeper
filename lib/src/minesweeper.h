/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file minesweeper.h
//! @brief Class that maintains the state and logic of the Minesweeper
//!        game.

#ifndef MINESWEEPER_H
#define MINESWEEPER_H

#include <vector>

// Forward declaration needed or class MinesweeperTest cannot be friend
// of class MS::Minesweeper
namespace Test
{
    class MinesweeperTest;
}

//! @namespace MS
//! @brief Minesweeper library scope
namespace MS
{
    //! @class Minesweeper
    //! @brief Class that maintains the state and logic of the
    //!        Minesweeper game.
    //!
    //! Minesweeper class maintains the game state by keeping account of
    //! board details (row and column count), mine locations, and square
    //! states on the board.
    //!
    //! There are three states in which any of the board squares may be
    //! during the game: <i>UNKNOWN</i>, <i>REVEALED</i>, and
    //! <i>FLAGGED</i>. Initially all squares are set in <i>UNKNOWN</i>
    //! state. Depending on the player's actions square states may
    //! change as shown in Figure below.
    //!
    //! @image html square_states.png "Square States"
    class Minesweeper
    {
    public:
        //! @brief Unit tests need access to private and protected
        //!        members.
        friend class Test::MinesweeperTest;

        //! @brief Square states.
        //!
        //! Any of the squares must be in one of the following states:
        enum States
        {
            UNKNOWN,  //!< Initial state
            REVEALED, //!< Opened square
            FLAGGED   //!< Square marked as containing a mine
        };

        //! @brief Class constructor.
        Minesweeper();

        //! @brief Class destructor.
        ~Minesweeper();

        //! @brief Game setup.
        //!
        //! Setup a new game. Setting the following game properties:
        //!
        //! <ul>
        //! <li>Set board size (rows and columns)</li>
        //! <li>Initialize states array</li>
        //! </ul>
        //!
        //! @param rows Row count
        //! @param cols Column count
        //! @param mines Mine count
        void setup(
            unsigned int rows,
            unsigned int cols,
            unsigned int mines
        );

        //! @brief Setup mines array.
        //!
        //! Mines array has the following properties:
        //!
        //! <ul>
        //! <li>Random mine locations</li>
        //! <li>No duplicate values</li>
        //! <li>Sorted in ascending order</li>
        //! <li>No mine at location 'first'</li>
        //! </ul>
        //!
        //! @param mines Mine count
        //! @param first First selected square
        void setMines(unsigned int mines, unsigned int first);

        //! @brief Check if the selected location contains a mine.
        //!
        //! @param location Selected location
        //! @return True, if selected location contains a mine, false
        //!         otherwise
        bool isMined(unsigned int location) const;

        //! @brief Get current square state at selected location.
        //!
        //! @param location Selected location
        //! @return Current square {@link MS::Minesweeper::States state}
        States getState(unsigned int location) const;

        //! @brief Toggle square flag state.
        //!
        //! Toggles square flag state from UNKNOWN to FLAGGED or from
        //! FLAGGED back to UNKNOWN. Does nothing if current square
        //! state is REVEALED.
        //!
        //! @param location Selected square
        //! @return New square state
        States toggleFlag(unsigned int location);

        //! @brief Get flagged squares.
        //!
        //! @return List of flagged squares.
        std::vector<unsigned int> getFlags() const;

        //! @brief Get adjacent squares for given location.
        //!
        //! Squares are ordered in increasing index order (i.e.
        //! NW, N, NE, W, E, SW, S, SE).
        //!
        //! @param location Location for which adjacent squares are
        //!        searched for
        //! @return List of adjacent squares
        std::vector<unsigned int> getAdjacentSquares(
            unsigned int location
        ) const;

        //! @brief Get mine count in squares surrounding the given
        //!        location.
        //!
        //! @param location Location for which surrounding mine count is
        //!        searched for
        //! @return Mine count in surrounding squares
        unsigned int getAdjacentMineCount(unsigned int location) const;

        //! @brief Select given location.
        //!
        //! Create a list of squares to be revealed together with given
        //! location. The list contains all surrounding squares free of
        //! mines.
        //!
        //! @param location Selected location
        //! @return Resulting list of locations
        std::vector<unsigned int> select(unsigned int location);

        //! @brief Check if the game has ended.
        //!
        //! Check number of UNKNOWN locations in states vector. The game
        //! has finished successfully if the number of UNKNOWN locations
        //! equals number of mines.
        //!
        //! @return true, if the game has finished successfully, false
        //!         otherwise
        bool isFinished() const;

    private:
        // Helper functions

        //! @brief Get distance between to locations on board.
        //!
        //! Calculate distance from location 'from' to location 'to'
        //! using Pythagoras' Theorem.
        //!
        //! @param from Start point
        //! @param to End point
        //! @return Distance between start and end points
        double getDistance(unsigned int from, unsigned int to) const;

        //! @brief Get surrounding squares for given list of squares.
        //!
        //! Get all adjacent squares surrounding the squares listed in
        //! parameter 'squares'. The new list of surrounding squares
        //! excludes squares already present in 'squares' as well as
        //! squares in other state than UNKNOWN.
        //!
        //! @param location Location for which surrounding squares are
        //!        searched for
        //! @param squares List of adjacent squares
        void getSurroundingSquares(
            unsigned int location,
            std::vector<unsigned int>& squares
        );

        //! @brief local attributes
        unsigned int rows_; //!< Row count
        unsigned int cols_; //!< Column count
        std::vector<unsigned int> mines_; //!< Mine location array
        std::vector<States> states_; //!< Square states array
    };
}

#endif // MINESWEEPER_H
