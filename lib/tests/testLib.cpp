/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include "testMinesweeper.h"
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>

int main(/*int argc, char *argv[]*/)
{
    // Informs test listener about test results
    CPPUNIT_NS::TestResult testResult;

    // Register listener for collecting the test results
    CPPUNIT_NS::TestResultCollector collectedResults;
    testResult.addListener(&collectedResults);

    // Register listener per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progressListener;
    testResult.addListener(&progressListener);

    // Insert test suite at test runner by registry
    CPPUNIT_NS::TestRunner testRunner;
    testRunner.addTest(
        CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
    testRunner.run(testResult);

    // Output results in compiler format
    CPPUNIT_NS::CompilerOutputter compilerOutputter(&collectedResults,
        std::cerr);
    compilerOutputter.write();

    // Return 0 if tests were successfull
    return collectedResults.wasSuccessful() ? 0 : 1;
}
