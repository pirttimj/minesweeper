/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file testMinesweeper.cpp

#include "testMinesweeper.h"
#include <algorithm>

//! @brief Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(Test::MinesweeperTest);

void Test::MinesweeperTest::setUp()
{
    srand((unsigned int)time(NULL));
    ms_ = new MS::Minesweeper();
}

void Test::MinesweeperTest::tearDown()
{
    delete ms_;
}

void Test::MinesweeperTest::testSetMines()
{
    unsigned int rows = 2;
    unsigned int cols = 2;
    unsigned int mines = 1;
    unsigned int first = rand() % (rows*cols);
    ms_->rows_ = rows;
    ms_->cols_ = cols;
    ms_->setMines(mines, first);

    mineArrayTests(rows, cols, mines, first);

    rows = 2;
    cols = 2;
    mines = 3;
    first = rand() % (rows*cols);
    ms_->rows_ = rows;
    ms_->cols_ = cols;
    ms_->setMines(mines, first);

    mineArrayTests(rows, cols, mines, first);

    rows = 4;
    cols = 4;
    mines = 10;
    first = rand() % (rows*cols);
    ms_->rows_ = rows;
    ms_->cols_ = cols;
    ms_->setMines(mines, first);

    mineArrayTests(rows, cols, mines, first);

    rows = 4;
    cols = 4;
    mines = 15;
    first = rand() % (rows*cols);
    ms_->rows_ = rows;
    ms_->cols_ = cols;
    ms_->setMines(mines, first);

    mineArrayTests(rows, cols, mines, first);
}

void Test::MinesweeperTest::testSetup()
{
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 3;

    // Setup board
    ms_->setup(rows, cols, mines);
    setupTests(rows, cols, mines);

    rows = 8;
    cols = 8;
    mines = 10;

    // Setup board
    ms_->setup(rows, cols, mines);
    setupTests(rows, cols, mines);

    rows = 5;
    cols = 5;
    mines = 6;

    // Setup board
    ms_->setup(rows, cols, mines);
    setupTests(rows, cols, mines);
}

void Test::MinesweeperTest::testIsMined()
{
    // 3x3 board, no mines
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {};
    isMinedTests(rows, cols, mines);

    // 3x3 board, mines at locations 0, 4, 8
    mines = {0, 4, 8};
    isMinedTests(rows, cols, mines);

    // 3x3 board, mine at every location
    mines = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    isMinedTests(rows, cols, mines);
}

void Test::MinesweeperTest::testGetState()
{
    // 1x3 board, no mines
    unsigned int rows = 1;
    unsigned int cols = 3;
    unsigned int mines = 0;
    std::vector<MS::Minesweeper::States> expected =
    {
        MS::Minesweeper::UNKNOWN,
        MS::Minesweeper::FLAGGED,
        MS::Minesweeper::REVEALED
    };
    std::stringstream msg;

    // Setup board
    ms_->setup(rows, cols, mines);
    // Set square states: 0: UNKNOWN, 1: FLAGGED, 2: REVEALED
    ms_->states_ = expected;

    for (unsigned int i = 0; i < ms_->states_.size(); i++)
    {
        MS::Minesweeper::States actual = ms_->getState(i);
        msg << "Function getState failed. Expected state at " << i
            << ": " << expected.at(i) << ", got: " << actual;

        CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected.at(i));

        msg.str("");
        msg.clear();
    }
}

void Test::MinesweeperTest::testToggleFlag()
{
    // 1x1 board (single square), no mines
    unsigned int rows = 1;
    unsigned int cols = 1;
    unsigned int mines = 0;
    std::stringstream msg;

    // Setup board
    ms_->setup(rows, cols, mines);

    // Test 1: Initial state must be UNKNOWN
    MS::Minesweeper::States actual = ms_->getState(0);
    msg << "Function toggleFlag failed. Expected initial state UNKNOWN"
        << ", got: " << actual;

    CPPUNIT_ASSERT_MESSAGE(msg.str(),
                           actual == MS::Minesweeper::UNKNOWN);

    msg.str("");
    msg.clear();

    // Test 2: Toggle state from UNKNOWN to FLAGGED
    actual = ms_->toggleFlag(0);
    msg << "Function toggleFlag failed. Expected toggled state FLAGGED,"
        << " got: " << actual;

    CPPUNIT_ASSERT_MESSAGE(msg.str(),
                           actual == MS::Minesweeper::FLAGGED);

    msg.str("");
    msg.clear();

    // Test 3: Toggle state back to UNKNOWN from FLAGGED
    actual = ms_->toggleFlag(0);
    msg << "Function toggleFlag failed. Expected toggled state UNKNOWN"
        << ", got: " << actual;

    CPPUNIT_ASSERT_MESSAGE(msg.str(),
                           actual == MS::Minesweeper::UNKNOWN);

    msg.str("");
    msg.clear();

    // Repeat tests 2 and 3

    // Test 4: Toggle state from UNKNOWN to FLAGGED
    actual = ms_->toggleFlag(0);
    msg << "Function toggleFlag failed. Expected toggled state FLAGGED,"
        << " got: " << actual;

    CPPUNIT_ASSERT_MESSAGE(msg.str(),
                           actual == MS::Minesweeper::FLAGGED);

    msg.str("");
    msg.clear();

    // Test 5: Toggle state back to UNKNOWN from FLAGGED
    actual = ms_->toggleFlag(0);
    msg << "Function toggleFlag failed. Expected toggled state UNKNOWN"
        << ", got: " << actual;

    CPPUNIT_ASSERT_MESSAGE(msg.str(),
                           actual == MS::Minesweeper::UNKNOWN);

    msg.str("");
    msg.clear();

    // Test 6: Change state to REVEALED and verify that toggleFlag
    //         function does not change the state
    ms_->states_.at(0) = MS::Minesweeper::REVEALED;
    actual = ms_->toggleFlag(0);

    msg << "Function toggleFlag failed. Expected toggled state REVEALED"
        << ", got: " << actual;

    CPPUNIT_ASSERT_MESSAGE(msg.str(),
                           actual == MS::Minesweeper::REVEALED);
}

void Test::MinesweeperTest::testGetFlags()
{
    unsigned int rows = 5;
    unsigned int cols = 5;
    unsigned int mines = 0; // No mines needed

    std::stringstream msg;
    std::vector<unsigned int> actual = {};
    std::vector<unsigned int> expected = {};

    // Setup board
    ms_->setup(rows, cols, mines);

    unsigned int i=0;
    for (i; i<ms_->rows_*ms_->cols_; ++i)
    {
        msg.str("");
        msg.clear();

        actual = ms_->getFlags();
        msg << "Function getFlags failed. Expected: "
            << vec2str(expected) << ", got: " << vec2str(actual)
            << ". Board: " << ms_->rows_ << "x" << ms_->cols_
            << "; i: " << i;

        CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);

        ms_->toggleFlag(i);
        expected.push_back(i);
    }

    msg.str("");
    msg.clear();

    actual = ms_->getFlags();
    msg << "Function getFlags failed. Expected: " << vec2str(expected)
        << ", got: " << vec2str(actual) << ". Board: " << ms_->rows_
        << "x" << ms_->cols_ << "; i: " << i;

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);

    // Flags on random locations
    msg.str("");
    msg.clear();

    ms_->setup(rows, cols, mines);

    ms_->toggleFlag(7);
    ms_->toggleFlag(3);
    ms_->toggleFlag(12);
    ms_->toggleFlag(1);
    ms_->toggleFlag(24);

    expected = {1, 3, 7, 12, 24};
    actual = ms_->getFlags();
    msg << "Function getFlags failed. Expected: " << vec2str(expected)
        << ", got: " << vec2str(actual) << ". Board: " << ms_->rows_
        << "x" << ms_->cols_;

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);
}

void Test::MinesweeperTest::testGetAdjacentSquares_3x3_at_0()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 0;
    unsigned int location = 0;
    std::vector<unsigned int> expected = {1, 3, 4};

    getAdjacentSquaresTests(rows, cols, location, expected);
}

void Test::MinesweeperTest::testGetAdjacentSquares_3x3_at_1()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 0;
    unsigned int location = 1;
    std::vector<unsigned int> expected = {0, 2, 3, 4, 5};

    getAdjacentSquaresTests(rows, cols, location, expected);
}

void Test::MinesweeperTest::testGetAdjacentSquares_3x3_at_2()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 0;
    unsigned int location = 2;
    std::vector<unsigned int> expected = {1, 4, 5};

    getAdjacentSquaresTests(rows, cols, location, expected);
}

void Test::MinesweeperTest::testGetAdjacentSquares_3x3_at_3()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 0;
    unsigned int location = 3;
    std::vector<unsigned int> expected = {0, 1, 4, 6, 7};

    getAdjacentSquaresTests(rows, cols, location, expected);
}

void Test::MinesweeperTest::testGetAdjacentSquares_3x3_at_4()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 0;
    unsigned int location = 4;
    std::vector<unsigned int> expected = {0, 1, 2, 3, 5, 6, 7, 8};

    getAdjacentSquaresTests(rows, cols, location, expected);
}

void Test::MinesweeperTest::testGetAdjacentSquares_3x3_at_5()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 0;
    unsigned int location = 5;
    std::vector<unsigned int> expected = {1, 2, 4, 7, 8};

    getAdjacentSquaresTests(rows, cols, location, expected);
}

void Test::MinesweeperTest::testGetAdjacentSquares_3x3_at_6()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 0;
    unsigned int location = 6;
    std::vector<unsigned int> expected = {3, 4, 7};

    getAdjacentSquaresTests(rows, cols, location, expected);
}

void Test::MinesweeperTest::testGetAdjacentSquares_3x3_at_7()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 0;
    unsigned int location = 7;
    std::vector<unsigned int> expected = {3, 4, 5, 6, 8};

    getAdjacentSquaresTests(rows, cols, location, expected);
}

void Test::MinesweeperTest::testGetAdjacentSquares_3x3_at_8()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    unsigned int mines = 0;
    unsigned int location = 8;
    std::vector<unsigned int> expected = {4, 5, 7};

    getAdjacentSquaresTests(rows, cols, location, expected);
}

void Test::MinesweeperTest::testGetAdjacentMineCount_3x3_0_mines()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {};
    unsigned int location = 4;

    getAdjacentMineCountTests(rows, cols, location, mines);
}

void Test::MinesweeperTest::testGetAdjacentMineCount_3x3_1_mines()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {0};
    unsigned int location = 4;

    getAdjacentMineCountTests(rows, cols, location, mines);
}

void Test::MinesweeperTest::testGetAdjacentMineCount_3x3_2_mines()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {0, 1};
    unsigned int location = 4;

    getAdjacentMineCountTests(rows, cols, location, mines);
}

void Test::MinesweeperTest::testGetAdjacentMineCount_3x3_3_mines()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {0, 1, 2};
    unsigned int location = 4;

    getAdjacentMineCountTests(rows, cols, location, mines);
}

void Test::MinesweeperTest::testGetAdjacentMineCount_3x3_4_mines()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {0, 1, 2, 3};
    unsigned int location = 4;

    getAdjacentMineCountTests(rows, cols, location, mines);
}

void Test::MinesweeperTest::testGetAdjacentMineCount_3x3_5_mines()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {0, 1, 2, 3, 5};
    unsigned int location = 4;

    getAdjacentMineCountTests(rows, cols, location, mines);
}

void Test::MinesweeperTest::testGetAdjacentMineCount_3x3_6_mines()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {0, 1, 2, 3, 5, 6};
    unsigned int location = 4;

    getAdjacentMineCountTests(rows, cols, location, mines);
}

void Test::MinesweeperTest::testGetAdjacentMineCount_3x3_7_mines()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {0, 1, 2, 3, 5, 6, 7};
    unsigned int location = 4;

    getAdjacentMineCountTests(rows, cols, location, mines);
}

void Test::MinesweeperTest::testGetAdjacentMineCount_3x3_8_mines()
{
    // 3x3 board
    unsigned int rows = 3;
    unsigned int cols = 3;
    std::vector<unsigned int> mines = {0, 1, 2, 3, 5, 6, 7, 8};
    unsigned int location = 4;

    getAdjacentMineCountTests(rows, cols, location, mines);
}

void Test::MinesweeperTest::testGetDistance_2x2()
{
    // 2x2 board
    unsigned int rows = 2;
    unsigned int cols = 2;
    std::vector<std::vector<double> > expected =
    {
        {
            0.0, 1.0, 1.0, sqrt(2)
        },
        {
            1.0, 0.0, sqrt(2), 1.0
        },
        {
            1.0, sqrt(2), 0.0, 1.0
        },
        {
            sqrt(2), 1.0, 1.0, 0.0
        }
    };

    getDistanceTests(rows, cols, expected);
}

void Test::MinesweeperTest::testGetDistance_3x2()
{
    // 3x2 board
    unsigned int rows = 3;
    unsigned int cols = 2;
    std::vector<std::vector<double> > expected =
    {
        {
            0.0, 1.0,
            1.0, sqrt(2),
            2.0, sqrt(pow(1, 2) + pow(2, 2))
        },
        {
            1.0, 0.0,
            sqrt(2), 1.0,
            sqrt(pow(1, 2) + pow(2, 2)), 2.0
        },
        {
            1.0, sqrt(2),
            0.0, 1.0,
            1.0, sqrt(2)
        },
        {
            sqrt(2), 1.0,
            1.0, 0.0,
            sqrt(2), 1.0
        },
        {
            2.0, sqrt(pow(1, 2) + pow(2, 2)),
            1.0, sqrt(2),
            0.0, 1.0
        },
        {
            sqrt(pow(1, 2) + pow(2, 2)), 2.0,
            sqrt(2), 1.0,
            1.0, 0.0
        }
    };

    getDistanceTests(rows, cols, expected);
}

void Test::MinesweeperTest::testGetDistance_2x3()
{
    // 2x3 board
    unsigned int rows = 2;
    unsigned int cols = 3;
    std::vector<std::vector<double> > expected =
    {
        {
            0.0, 1.0, 2.0,
            1.0, sqrt(2), sqrt(pow(1, 2) + pow(2, 2))
        },
        {
            1.0, 0.0, 1.0,
            sqrt(2), 1.0, sqrt(2)
        },
        {
            2.0, 1.0, 0.0,
            sqrt(pow(1, 2) + pow(2, 2)), sqrt(2), 1.0
        },
        {
            1.0, sqrt(2), sqrt(pow(1, 2) + pow(2, 2)),
            0.0, 1.0, 2.0
        },
        {
            sqrt(2), 1.0, sqrt(2),
            1.0, 0.0, 1.0
        },
        {
            sqrt(pow(1, 2) + pow(2, 2)), sqrt(2), 1.0,
            2.0, 1.0, 0.0
        }
    };

    getDistanceTests(rows, cols, expected);
}

void Test::MinesweeperTest::testSelect_4x5_mines_at_0_1_5_12_19()
{
    unsigned int rows = 4;
    unsigned int cols = 5;
    std::vector<unsigned int> mines = {0, 1, 5, 12, 19};
    std::vector<unsigned int> flags = {};
    std::vector<std::vector<unsigned int> > expected =
    {
        {0},
        {1},
        {2},
        {2, 3, 4, 7, 8, 9, 13, 14},
        {2, 3, 4, 7, 8, 9, 13, 14},
        {5},
        {6},
        {7},
        {8},
        {2, 3, 4, 7, 8, 9, 13, 14},
        {10},
        {11},
        {12},
        {13},
        {14},
        {10, 11, 15, 16},
        {16},
        {17},
        {18},
        {19}
    };

    selectTests(rows, cols, mines, flags, expected);
}

void Test::MinesweeperTest::
testSelect_5x5_mines_at_2_7_12_17_22_flags_at_12_13_14()
{
    unsigned int rows = 5;
    unsigned int cols = 5;
    std::vector<unsigned int> mines = {2, 7, 12, 17, 22};
    std::vector<unsigned int> flags = {12, 13, 14};
    std::vector<std::vector<unsigned int> > expected =
    {
        {0, 1, 5, 6, 10, 11, 15, 16, 20, 21},
        {1},
        {2},
        {3},
        {3, 4, 8, 9},
        {0, 1, 5, 6, 10, 11, 15, 16, 20, 21},
        {6},
        {7},
        {8},
        {3, 4, 8, 9},
        {0, 1, 5, 6, 10, 11, 15, 16, 20, 21},
        {11},
        {},
        {},
        {},
        {0, 1, 5, 6, 10, 11, 15, 16, 20, 21},
        {16},
        {17},
        {18},
        {18, 19, 23, 24},
        {0, 1, 5, 6, 10, 11, 15, 16, 20, 21},
        {21},
        {22},
        {23},
        {18, 19, 23, 24}
    };

    selectTests(rows, cols, mines, flags, expected);
}

void Test::MinesweeperTest::testIsFinished_success()
{
    unsigned int rows = 1;
    unsigned int cols = 5;
    std::vector<unsigned int> mines = {1, 3};
    std::vector<std::pair<unsigned int, bool> > data =
    {
        {0, false},
        {2, false},
        {4, true}
    };

    isFinishedTests(rows, cols, mines, data);
}

void Test::MinesweeperTest::testIsFinished_failure()
{
    unsigned int rows = 1;
    unsigned int cols = 5;
    std::vector<unsigned int> mines = {1, 3};
    std::vector<std::pair<unsigned int, bool> > data =
    {
        {0, false},
        {2, false},
        {1, true}
    };

    isFinishedTests(rows, cols, mines, data);
}

void Test::MinesweeperTest::testIsFinished_2x2_mines_at_1()
{
    unsigned int rows = 2;
    unsigned int cols = 2;
    std::vector<unsigned int> mines = {1};
    bool actual;
    std::stringstream msg;

    // Board setup
    ms_->setup(2, 2, 0);
    // Set mines
    ms_->mines_ = mines;

    // Click at 3
    ms_->select(3);
    actual = ms_->isFinished();
    msg.str("");
    msg.clear();
    msg << "Function isFinished failed. Expected: false, got: "
        << std::boolalpha << actual << ". Board setup: " << ms_->rows_
        << "x" << ms_->cols_ << ", mines at: " << vec2str(ms_->mines_)
        << ". Actions: click at 3.";

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == false);

    // Click at 2
    ms_->select(2);
    actual = ms_->isFinished();
    msg.str("");
    msg.clear();
    msg << "Function isFinished failed. Expected: false, got: "
        << std::boolalpha << actual << ". Board setup: " << ms_->rows_
        << "x" << ms_->cols_ << ", mines at: " << vec2str(ms_->mines_)
        << ". Actions: click at 3, 2.";

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == false);

    // Set flag at 1
    ms_->toggleFlag(1);
    actual = ms_->isFinished();
    msg.str("");
    msg.clear();
    msg << "Function isFinished failed. Expected: false, got: "
        << std::boolalpha << actual << ". Board setup: " << ms_->rows_
        << "x" << ms_->cols_ << ", mines at: " << vec2str(ms_->mines_)
        << ". Actions: click at 3, 2. Set flag at 1.";

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == false);

    // Click at 0
    ms_->select(0);
    actual = ms_->isFinished();
    msg.str("");
    msg.clear();
    msg << "Function isFinished failed. Expected: false, got: "
        << std::boolalpha << actual << ". Board setup: " << ms_->rows_
        << "x" << ms_->cols_ << ", mines at: " << vec2str(ms_->mines_)
        << ". Actions: click at 3, 2. Set flag at 1. Click at 0.";

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == true);

}

void Test::MinesweeperTest::testGame_8x8_0()
{
    unsigned int rows = 8;
    unsigned int cols = 8;
    const std::vector<unsigned int> mines =
    {
        4, 16, 26, 32, 42, 47, 49, 52, 58, 59
    };
    std::vector<unsigned int> adjacentMines =
    {
        0, 0, 0, 1, 0, 1, 0, 0,
        1, 1, 0, 1, 1, 1, 0, 0,
        0, 2, 1, 1, 0, 0, 0, 0,
        2, 3, 0, 1, 0, 0, 0, 0,
        0, 3, 2, 2, 0, 0, 1, 1,
        2, 3, 1, 2, 1, 1, 1, 0,
        1, 2, 4, 4, 1, 1, 1, 1,
        1, 2, 2, 2, 2, 1, 0, 0
    };
    std::vector<TestAction> actions =
    {
        {
            REVEAL, 1, false, {1, 0, 2, 9, 8, 10, 3, 17, 11, 18, 19}
        },
        {
            REVEAL, 20, false,
            {
                20, 12, 21, 28, 13, 27, 29, 22, 36, 14, 30, 35, 37, 38,
                5, 6, 23, 44, 15, 31, 43, 45, 7, 39, 46
            }
        },
        {
            REVEAL, 34, false, {34}
        },
        {
            REVEAL, 25, false, {25}
        },
        {
            REVEAL, 24, false, {24}
        },
        {
            REVEAL, 33, false, {33}
        },
        {
            REVEAL, 41, false, {41}
        },
        {
            REVEAL, 40, false, {40}
        },
        {
            REVEAL, 55, false, {55}
        },
        {
            REVEAL, 54, false, {54}
        },
        {
            REVEAL, 63, false, {63, 62, 61, 53}
        },
        {
            REVEAL, 51, false, {51}
        },
        {
            REVEAL, 60, false, {60}
        },
        {
            REVEAL, 58, false, {58}
        },
        {
            REVEAL, 56, false, {56}
        },
        {
            REVEAL, 48, false, {48}
        },
        {
            REVEAL, 57, true, {57}
        }
    };

    gameTests(rows, cols, mines, adjacentMines, actions);
}
void Test::MinesweeperTest::testGame_8x8_1()
{
    unsigned int rows = 8;
    unsigned int cols = 8;
    const std::vector<unsigned int> mines =
    {
        0, 7, 12, 23, 24, 25, 29, 38, 53, 59
    };
    const std::vector<unsigned int> adjacentMines =
    {
        0, 1, 0, 1, 1, 1, 1, 0,
        1, 1, 0, 1, 0, 1, 2, 2,
        2, 2, 1, 1, 2, 2, 2, 0,
        1, 1, 1, 0, 1, 1, 3, 2,
        2, 2, 1, 0, 1, 2, 1, 1,
        0, 0, 0, 0, 1, 2, 2, 1,
        0, 0, 1, 1, 2, 0, 1, 0,
        0, 0, 1, 0, 2, 1, 1, 0
    };
    std::vector<TestAction> actions =
    {
        {
            REVEAL, 10, false, {10, 2, 9, 11, 18, 1, 3, 17, 19}
        },
        {
            REVEAL, 20, false, {20}
        },
        {
            TOGGLE, 48, false, {}
        },
        {
            TOGGLE, 49, false, {}
        },
        {
            REVEAL, 16, false, {16}
        },
        {
            REVEAL, 26, false, {26}
        },
        {
            REVEAL, 34, false, {34}
        },
        {
            REVEAL, 33, false, {33}
        },
        {
            REVEAL, 35, false,
            {
                35, 27, 36, 43, 28, 42, 44, 51, 41, 50, 52, 32, 40
            }
        },
        {
            REVEAL, 8, false, {8}
        },
        {
            REVEAL, 4, false, {4}
        },
        {
            REVEAL, 5, false, {5}
        },
        {
            REVEAL, 13, false, {13}
        },
        {
            REVEAL, 21, false, {21}
        },
        {
            REVEAL, 14, false, {14}
        },
        {
            REVEAL, 6, false, {6}
        },
        {
            TOGGLE, 48, false, {}
        },
        {
            REVEAL, 48, false, {48, 56, 57, 58}
        },
        {
            TOGGLE, 49, false, {}
        },
        {
            REVEAL, 49, false, {49}
        },
        {
            REVEAL, 22, false, {22}
        },
        {
            REVEAL, 30, false, {30}
        },
        {
            REVEAL, 31, false, {31}
        },
        {
            REVEAL, 15, false, {15}
        },
        {
            REVEAL, 37, false, {37}
        },
        {
            REVEAL, 45, false, {45}
        },
        {
            REVEAL, 63, false, {63, 55, 62, 54, 47, 46}
        },
        {
            REVEAL, 39, false, {39}
        },
        {
            REVEAL, 61, false, {61}
        },
        {
            REVEAL, 60, true, {60}
        }
    };

    gameTests(rows, cols, mines, adjacentMines, actions);
}

////////////////////////////////////////////////////////////////////////
// Helper methods
////////////////////////////////////////////////////////////////////////

void Test::MinesweeperTest::mineArrayTests(
    unsigned int rows,
    unsigned int cols,
    unsigned int mines,
    unsigned int first
    )
{
    std::stringstream msg;

    // Test mines array size
    {
        unsigned int expected = mines;
        unsigned int actual = ms_->mines_.size();
        msg << "Mine array size mismatch. Expected: " << expected
            << ", got: " << actual << ".";

        CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);
    }

    // Test if mines array is sorted in ascending order
    {
        msg.str("");
        msg.clear();
        msg << "Mine array not sorted.";

        CPPUNIT_ASSERT_MESSAGE(
            msg.str(),
            std::is_sorted(ms_->mines_.begin(), ms_->mines_.end())
        );
    }

    // Test if mine array contains duplicate values
    {
        msg.str("");
        msg.clear();
        msg << "Duplicate values in mine array: ";
        std::vector<unsigned int>::const_iterator current =
            ms_->mines_.begin();
        std::vector<unsigned int>::const_iterator end =
            ms_->mines_.end();
        while (current != end)
        {
            CPPUNIT_ASSERT_MESSAGE(
                msg.str() + vec2str(ms_->mines_),
                std::find(current + 1, end, *current) == end
            );

            ++current;
        }
    }

    // Test mine locations
    {
        msg.str("");
        msg.clear();
        msg << "Illegal mine location (0-" << rows*cols-1 << "): ";

        for (auto& i : ms_->mines_)
        {
            // Mine locations must be within range 0 to (rows*cols-1)
            CPPUNIT_ASSERT_MESSAGE(
                msg.str() + std::to_string(i), i < (rows * cols)
            );
        }

        // No mine at location 'first'
        msg.str("");
        msg.clear();
        msg << "First selected location (" << first << ") must be "
            << "free from mines. Mine array: " << vec2str(ms_->mines_);

        CPPUNIT_ASSERT_MESSAGE(
            msg.str(), std::find(ms_->mines_.begin(),
                                 ms_->mines_.end(),
                                 first) == ms_->mines_.end()
        );
    }
}

void Test::MinesweeperTest::setupTests(
    unsigned int rows,
    unsigned int cols,
    unsigned int mines
    )
{
    unsigned int expected;
    unsigned int actual;
    std::stringstream msg;

    // Board setup
    ms_->setup(rows, cols, mines);
    unsigned int first = rand() % (ms_->rows_*ms_->cols_);
    ms_->setMines(mines, first);

    // Mine array tests
    mineArrayTests(rows, cols, mines, first);

    // Check rows
    expected = rows;
    actual = ms_->rows_;
    msg << "Function setup failed to set rows. Expected: " << expected
        << " rows, got: " << actual << " rows.";

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);

    // Check columns
    expected = cols;
    actual = ms_->cols_;
    msg.str("");
    msg.clear();
    msg << "Function setup failed to set cols. Expected: " << expected
        << " cols, got: " << actual << " cols.";

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);

    // States array size
    expected = rows*cols;
    actual = ms_->states_.size();
    msg.str("");
    msg.clear();
    msg << "Function setup failed to set states array size. Expected "
        << "size: " << expected << ", got: " << actual << ".";

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);

    // States array contents
    for (auto square: ms_->states_)
    {
        msg.str("");
        msg.clear();
        msg << "Function setup failed to set states array contents."
            << "Expected UNKNOWN, got: " << square << ".";

        CPPUNIT_ASSERT_MESSAGE(msg.str(),
                               square == MS::Minesweeper::UNKNOWN);
    }
}

void Test::MinesweeperTest::isMinedTests(
    unsigned int rows,
    unsigned int cols,
    std::vector<unsigned int> mines
    )
{
    // Setup board
    ms_->mines_.clear();
    ms_->rows_ = rows;
    ms_->cols_ = cols;
    ms_->mines_ = mines;
    std::stringstream msg;

    // Check mine locations
    for (auto &location : ms_->mines_)
    {
        bool actual = ms_->isMined(location);
        bool expected = (std::find(std::begin(mines), std::end(mines),
                                   location) != mines.end());
        msg << "Function isMined failed at " << location
            << ": expected: " << (expected ? "" : "NOT ") << "a mine, "
            << "got: " << (actual ? "" : "NOT ") << "a mine. Mines at: "
            << vec2str(ms_->mines_);

        CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);
    }
}

void Test::MinesweeperTest::getAdjacentSquaresTests(
    unsigned int rows,
    unsigned int cols,
    unsigned int location,
    std::vector<unsigned int> expected
    )
{
    // Board setup
    ms_->rows_ = rows;
    ms_->cols_ = cols;
    ms_->mines_.clear();
    ms_->setup(rows, cols, 0);

    std::vector<unsigned int> actual =
        ms_->getAdjacentSquares(location);
    std::stringstream msg;
    msg << "Function getAdjacentSquares failed at " << location
        << ": expected: " << vec2str(expected) << ", got: "
        << vec2str(actual) << ", board: " << ms_->rows_ << "x"
        << ms_->cols_;

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);
}

void Test::MinesweeperTest::getAdjacentMineCountTests(
    unsigned int rows,
    unsigned int cols,
    unsigned int location,
    std::vector<unsigned int> mines
    )
{
    // Board setup
    ms_->rows_ = rows;
    ms_->cols_ = cols;
    ms_->mines_.clear();
    ms_->mines_ = mines;

    unsigned int expected = mines.size();
    unsigned int actual = ms_->getAdjacentMineCount(location);
    std::stringstream msg;
    msg << "Function getAdjacentMineCount failed. Expected: "
        << expected << " mines, got: " << actual << " mines. Board "
        << "setup: " << ms_->rows_ << "x" << ms_->cols_ << ", "
        << "location: " << location << ", mines at " << vec2str(mines);

    CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);
}

void Test::MinesweeperTest::getDistanceTests(
    unsigned int rows,
    unsigned int cols,
    const std::vector<std::vector<double> >& expected
    )
{
    // Board setup, 0 mines
    ms_->setup(rows, cols, 0);

    std::stringstream msg;

    // Looping from each square (from) to each other (to) square on
    // board
    for (unsigned int from = 0; from < rows*cols; ++from)
    {
        for (unsigned int to = 0; to < rows*cols; ++to)
        {
            msg.str("");
            msg.clear();
            double actual = ms_->getDistance(from, to);
            msg << std::fixed
                << "Function getDistance failed to calculate the "
                << "distance between points " << from << " -> " << to
                << ". Expected: " << expected[from][to] << ", got: "
                << actual << ". Board: " << rows << "x" << cols << ".";

            CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(
                msg.str(), expected[from][to], actual, 0.00000001);
        }
    }
}

void Test::MinesweeperTest::selectTests(
    unsigned int rows,
    unsigned int cols,
    const std::vector<unsigned int>& mines,
    const std::vector<unsigned int>& flags,
    const std::vector<std::vector<unsigned int> >& expected
    )
{
    // Board setup, initially no mines
    ms_->setup(rows, cols, 0);
    ms_->mines_ = mines; // Set mines
    std::vector<unsigned int> actual;
    std::stringstream msg;

    for (unsigned int i = 0; i < ms_->states_.size(); ++i)
    {
        // Reset states vector to UNKNOWN before each test
        std::fill(ms_->states_.begin(), ms_->states_.end(),
                  MS::Minesweeper::States::UNKNOWN);
        for (auto i : flags)
        {
            ms_->states_.at(i) = MS::Minesweeper::States::FLAGGED;
        }
        msg.str("");
        msg.clear();

        // Test vector size
        unsigned int expectedSize = expected[i].size();
        actual = ms_->select(i);
        unsigned int actualSize = actual.size();

        msg << "Function select failed at location " << i
            << ". Expected size: " << expectedSize << ", got: "
            << actualSize  << ". Board setup: " << ms_->rows_
            << "x" << ms_->cols_ << ", mines at: "
            << vec2str(ms_->mines_) << ", flags at: " << vec2str(flags);

        CPPUNIT_ASSERT_MESSAGE(msg.str(), actualSize == expectedSize);

        // Test vector contents
        msg.str("");
        msg.clear();

        // Sort contents before test
        std::sort(actual.begin(), actual.end());

        msg << "Function select failed at location " << i
            << ". Expected list: " << vec2str(expected[i]) << ", got: "
            << vec2str(actual) << ". Board setup: " << ms_->rows_
            << "x" << ms_->cols_ << ", mines at: "
            << vec2str(ms_->mines_) << ", flags at: " << vec2str(flags);

        CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected[i]);
    }
}

void Test::MinesweeperTest::isFinishedTests(
    unsigned int rows,
    unsigned int cols,
    const std::vector<unsigned int>& mines,
    const std::vector<std::pair<unsigned int, bool> > data
    )
{
    // Board setup, initially no mines
    ms_->setup(rows, cols, 0);
    ms_->mines_ = mines; // Set mines
    bool actual;
    std::stringstream msg;

    for (auto i : data)
    {
        ms_->select(i.first);
        actual = ms_->isFinished();
        msg.str("");
        msg.clear();
        msg << "Function isFinished failed at location " << i.first
            << ". Expected: " << std::boolalpha << i.second << ", got: "
            << actual << ". Board setup: " << ms_->rows_ << "x"
            << ms_->cols_ << ", mines at: " << vec2str(ms_->mines_);

        CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == i.second);
    }
}

void Test::MinesweeperTest::gameTests(
    unsigned int rows,
    unsigned int cols,
    const std::vector<unsigned int>& mines,
    const std::vector<unsigned int>& adjacentMines,
    std::vector<TestAction>& actions
    )
{
    // Board setup, initially no mines
    ms_->setup(rows, cols, 0);
    ms_->mines_ = mines; // Set mines
    std::stringstream msg;
    std::vector<MS::Minesweeper::States> expectedStates(rows*cols);
    std::fill(expectedStates.begin(),
              expectedStates.end(),
              MS::Minesweeper::States::UNKNOWN);

    // Test that states vector is properly initialized
    std::vector<MS::Minesweeper::States>::iterator it;
    for (it = ms_->states_.begin(); it != ms_->states_.end(); ++it)
    {
        msg.str("");
        msg.clear();
        msg << "Game initialization failed. States vector at "
            << (it - ms_->states_.begin()) << ": " << *it
            << "; expected: UNKNOWN.";

        CPPUNIT_ASSERT_MESSAGE(msg.str(),
                               *it == MS::Minesweeper::States::UNKNOWN);
    }

    // Test adjacent mine count
    for (unsigned int i = 0; i < ms_->rows_*ms_->cols_; ++i)
    {
        msg.str("");
        msg.clear();
        unsigned int actual = ms_->getAdjacentMineCount(i);
        unsigned int expected = adjacentMines.at(i);
        msg << "Game initialization failed. Expected adjacent mine "
            << "count at " << i << ": " << expected << ", got: "
            << actual << ".";

        CPPUNIT_ASSERT_MESSAGE(msg.str(), actual == expected);
    }

    // Game play test
    for (auto &action : actions)
    {
        std::vector<unsigned int> selection = {};

        if (action.action == REVEAL)
        {
            // Test select function
            selection = ms_->select(action.location);
            std::sort(selection.begin(), selection.end());
            std::sort((action.selection).begin(),
                      (action.selection).end());
            msg.str("");
            msg.clear();
            msg << "Game failed on action " << action.action
                << " at location " << action.location
                << ". Expected select to return: "
                << vec2str(action.selection) << ", got: "
                << vec2str(selection) << ". Board: " << ms_->rows_
                << "x" << ms_->cols_ << ", mines at: "
                << vec2str(ms_->mines_);

            CPPUNIT_ASSERT_MESSAGE(msg.str(),
                                   selection == action.selection);

            // Test that states vector has been modified
            for (auto &i : selection)
            {
                // Modify expectedStates vector
                expectedStates.at(i)
                    = MS::Minesweeper::States::REVEALED;
            }
        }
        else if (action.action == TOGGLE)
        {
            // Toggle flag
            expectedStates.at(action.location)
                = (expectedStates.at(action.location)
                    == MS::Minesweeper::States::UNKNOWN)
                ? MS::Minesweeper::States::FLAGGED
                : MS::Minesweeper::States::UNKNOWN;
            ms_->toggleFlag(action.location);
        }
        else
        {
            // ERROR: Unknown action
        }

        msg.str("");
        msg.clear();
        msg << "Game failed on action " << action.action
            << " at location " << action.location
            << ": states vector failure.";

        CPPUNIT_ASSERT_MESSAGE(msg.str(),
                               ms_->states_ == expectedStates);

        // Test isFinished function
        bool finished = ms_->isFinished();
        msg.str("");
        msg.clear();
        msg << "Game failed on action " << action.action
            << " at location " << action.location
            << ". Expected isFinished to return " << std::boolalpha
            << action.finished << ", got: " << finished << " Board: "
            << ms_->rows_ << "x" << ms_->cols_ << ", mines at: "
            << vec2str(ms_->mines_);

        CPPUNIT_ASSERT_MESSAGE(msg.str(), finished == action.finished);
    }
}

std::string Test::MinesweeperTest::vec2str(
    const std::vector<unsigned int>& v
    ) const
{
    std::stringstream msg;
    std::vector<unsigned int>::const_iterator it = v.begin();

    msg << "[";
    while (it != v.end())
    {
        msg << *it;
        ++it;

        if (it != v.end())
        {
            msg << ", ";
        }
    }

    msg << "]";

    return msg.str();
}

std::ostream& operator<<(
    std::ostream& os,
    const MS::Minesweeper::States& state
    )
{
    switch (state)
    {
    case MS::Minesweeper::States::UNKNOWN:
        os << "UNKNOWN";
        break;
    case MS::Minesweeper::States::FLAGGED:
        os << "FLAGGED";
        break;
    case MS::Minesweeper::States::REVEALED:
        os << "REVEALED";
        break;
    default:
        os << "???";
    }

    return os;
}
