/***********************************************************************
 * Copyright 2016 Juha Pirttimäki
 *
 * This file is part of Minesweeper.
 *
 *   Minesweeper is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published
 *   by the Free Software Foundation, either version 3 of the License,
 *   or (at your option) any later version.
 *
 *   Minesweeper is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Minesweeper. If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

//! @file testMinesweeper.h
//! @brief Library Minesweeper unit tests.

#ifndef TEST_MINESWEEPER_H
#define TEST_MINESWEEPER_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include "minesweeper.h"

//! @namespace Test
//! @brief Minesweeper unit tests scope
namespace Test
{
    //! @class MinesweeperTest
    //! @brief Library Minesweeper unit tests.
    class MinesweeperTest : public CPPUNIT_NS::TestFixture
    {
        CPPUNIT_TEST_SUITE(Test::MinesweeperTest);

        CPPUNIT_TEST(testSetup);
        CPPUNIT_TEST(testSetMines);
        CPPUNIT_TEST(testIsMined);
        CPPUNIT_TEST(testGetState);
        CPPUNIT_TEST(testToggleFlag);
        CPPUNIT_TEST(testGetFlags);

        CPPUNIT_TEST(testGetAdjacentSquares_3x3_at_0);
        CPPUNIT_TEST(testGetAdjacentSquares_3x3_at_1);
        CPPUNIT_TEST(testGetAdjacentSquares_3x3_at_2);
        CPPUNIT_TEST(testGetAdjacentSquares_3x3_at_3);
        CPPUNIT_TEST(testGetAdjacentSquares_3x3_at_4);
        CPPUNIT_TEST(testGetAdjacentSquares_3x3_at_5);
        CPPUNIT_TEST(testGetAdjacentSquares_3x3_at_6);
        CPPUNIT_TEST(testGetAdjacentSquares_3x3_at_7);
        CPPUNIT_TEST(testGetAdjacentSquares_3x3_at_8);

        CPPUNIT_TEST(testGetAdjacentMineCount_3x3_0_mines);
        CPPUNIT_TEST(testGetAdjacentMineCount_3x3_1_mines);
        CPPUNIT_TEST(testGetAdjacentMineCount_3x3_2_mines);
        CPPUNIT_TEST(testGetAdjacentMineCount_3x3_3_mines);
        CPPUNIT_TEST(testGetAdjacentMineCount_3x3_4_mines);
        CPPUNIT_TEST(testGetAdjacentMineCount_3x3_5_mines);
        CPPUNIT_TEST(testGetAdjacentMineCount_3x3_6_mines);
        CPPUNIT_TEST(testGetAdjacentMineCount_3x3_7_mines);
        CPPUNIT_TEST(testGetAdjacentMineCount_3x3_8_mines);

        CPPUNIT_TEST(testGetDistance_2x2);
        CPPUNIT_TEST(testGetDistance_3x2);
        CPPUNIT_TEST(testGetDistance_2x3);

        CPPUNIT_TEST(testSelect_4x5_mines_at_0_1_5_12_19);
        CPPUNIT_TEST(
            testSelect_5x5_mines_at_2_7_12_17_22_flags_at_12_13_14
        );

        CPPUNIT_TEST(testIsFinished_success);
        CPPUNIT_TEST(testIsFinished_failure);
        CPPUNIT_TEST(testIsFinished_2x2_mines_at_1);

        CPPUNIT_TEST(testGame_8x8_0);
        CPPUNIT_TEST(testGame_8x8_1);

        CPPUNIT_TEST_SUITE_END();

    public:
        //! @brief Set up context before running a test.
        void setUp();

        //! @brief Clean up after the test run.
        void tearDown();

        // Test methods

        //! @brief Test {@link MS::Minesweeper::setup setup} function.
        //!
        //! Validate setup:
        //! <ul>
        //! <li>Board size</li>
        //! <li>Mine array</li>
        //! <li>Status array</li>
        //! </ul>
        void testSetup();

        //! @brief Test setMines function.
        //!
        //! Validate mine array contents:
        //!
        //! <ul>
        //! <li>Mine array size</li>
        //! <li>Sorted in ascending order</li>
        //! <li>No duplicate values</li>
        //! <li>Mine locations (must be within range 0 to
        //!     rows*cols-1)</li>
        //! <li>No mines at first selected square</li>
        //! </ul>
        void testSetMines();

        //! @brief Test {@link MS::Minesweeper::isMined isMined}
        //!        function.
        //!
        //! Setup board with mines at random locations. Check if isMined
        //! function returns correct state at each board location.
        void testIsMined();

        //! @brief Test {@link MS::Minesweeper::getState getState}
        //!        function.
        //!
        //! Setup 1x3 board and set state for each square on board as
        //! 0: UNKNOWN, 1: FLAGGED, 2: REVEALED. Check if getState
        //! function returns correct state at each board location.
        void testGetState();

        //! @brief Test {@link MS::Minesweeper::toggleFlag toggleFlag}
        //!        function.
        //!
        //! Setup 1x1 board. Test toggleFlag function with the following
        //! sequences:
        //!
        //! <table>
        //! <tr>
        //! <th>Initial State</th><th>Next State</th><th>Next State</th>
        //! <th>Next State</th></tr>
        //! <tr>
        //! <td>UNKNOWN</td><td>FLAGGED</td><td>UNKNOWN</td>
        //! <td>FLAGGED</td>
        //! </tr>
        //! </table>
        //! <table>
        //! <tr>
        //! <th>Initial State</th><th>Next State</th>
        //! </tr>
        //! <tr>
        //! <td>REVEALED</td><td>REVEALED</td>
        //! </tr>
        //! </table>
        void testToggleFlag();

        //! @brief Test {@link MS::Minesweeper::getFlags getFlags}
        //!        function.
        //!
        //! Setup 5x5 board. Set flags in loop and test getFlags
        //! function.
        void testGetFlags();

        //! @brief Test {@link MS::Minesweeper::getAdjacentSquares
        //!        getAdjacentSquares} function.
        //!
        //! @test Adjacent squares for index 0 on a 3x3 board returns
        //! 1, 3, 4.
        //! @image html
        //! board_3x3_select_0.png "Adjacent Squares at index 0"
        void testGetAdjacentSquares_3x3_at_0();

        //! @brief Test {@link MS::Minesweeper::getAdjacentSquares
        //!        getAdjacentSquares} function.
        //!
        //! @test Adjacent squares for index 1 on a 3x3 board returns
        //! 0, 2, 3, 4, 5.
        //! @image html
        //! board_3x3_select_1.png "Adjacent Squares at index 1"
        void testGetAdjacentSquares_3x3_at_1();

        //! @brief Test {@link MS::Minesweeper::getAdjacentSquares
        //!        getAdjacentSquares} function.
        //!
        //! @test Adjacent squares for index 2 on a 3x3 board returns
        //! 1, 4, 5.
        //! @image html
        //! board_3x3_select_2.png "Adjacent Squares at index 2"
        void testGetAdjacentSquares_3x3_at_2();

        //! @brief Test {@link MS::Minesweeper::getAdjacentSquares
        //!        getAdjacentSquares} function.
        //!
        //! @test Adjacent squares for index 3 on a 3x3 board returns
        //! 0, 1, 4, 6, 7.
        //! @image html
        //! board_3x3_select_3.png "Adjacent Squares at index 3"
        void testGetAdjacentSquares_3x3_at_3();

        //! @brief Test {@link MS::Minesweeper::getAdjacentSquares
        //!        getAdjacentSquares} function.
        //!
        //! @test Adjacent squares for index 4 on a 3x3 board returns
        //! 0, 1, 2, 3, 5, 6, 7, 8.
        //! @image html
        //! board_3x3_select_4.png "Adjacent Squares at index 4"
        void testGetAdjacentSquares_3x3_at_4();

        //! @brief Test {@link MS::Minesweeper::getAdjacentSquares
        //!        getAdjacentSquares} function.
        //!
        //! @test Adjacent squares for index 5 on a 3x3 board returns
        //! 1, 2, 4, 7, 8.
        //! @image html
        //! board_3x3_select_5.png "Adjacent Squares at index 5"
        void testGetAdjacentSquares_3x3_at_5();

        //! @brief Test {@link MS::Minesweeper::getAdjacentSquares
        //!        getAdjacentSquares} function.
        //!
        //! @test Adjacent squares for index 6 on a 3x3 board returns
        //! 3, 4, 7.
        //! @image html
        //! board_3x3_select_6.png "Adjacent Squares at index 6"
        void testGetAdjacentSquares_3x3_at_6();

        //! @brief Test {@link MS::Minesweeper::getAdjacentSquares
        //!        getAdjacentSquares} function.
        //!
        //! @test Adjacent squares for index 7 on a 3x3 board returns
        //! 3, 4, 5, 6, 8.
        //! @image html
        //! board_3x3_select_7.png "Adjacent Squares at index 7"
        void testGetAdjacentSquares_3x3_at_7();

        //! @brief Test {@link MS::Minesweeper::getAdjacentSquares
        //!        getAdjacentSquares} function.
        //!
        //! @test Adjacent squares for index 8 on a 3x3 board returns
        //! 4, 5, 7.
        //! @image html
        //! board_3x3_select_8.png "Adjacent Squares at index 8"
        void testGetAdjacentSquares_3x3_at_8();

        //! @brief Test {@link MS::Minesweeper::getAdjacentMineCount
        //! getAdjacentMineCount} function.
        //!
        //! @test 3x3 board, no mines, testing location index 4.<br>
        //!       Adjacent mine count returns 0.
        //! @image html board_3x3_0_mines.png ""
        void testGetAdjacentMineCount_3x3_0_mines();

        //! @brief Test {@link MS::Minesweeper::getAdjacentMineCount
        //! getAdjacentMineCount} function.
        //!
        //! @test 3x3 board, 1 mine at location 0, testing location
        //!       index 4.<br>Adjacent mine count returns 1.
        //! @image html board_3x3_1_mines.png ""
        void testGetAdjacentMineCount_3x3_1_mines();

        //! @brief Test {@link MS::Minesweeper::getAdjacentMineCount
        //! getAdjacentMineCount} function.
        //!
        //! @test 3x3 board, 2 mines at locations 0, 1, testing location
        //!       index 4.<br>Adjacent mine count returns 2.
        //! @image html board_3x3_2_mines.png ""
        void testGetAdjacentMineCount_3x3_2_mines();

        //! @brief Test {@link MS::Minesweeper::getAdjacentMineCount
        //! getAdjacentMineCount} function.
        //!
        //! @test 3x3 board, 3 mines at locations 0, 1, 2, testing
        //!       location index 4.<br> Adjacent mine count returns 3.
        //! @image html board_3x3_3_mines.png ""
        void testGetAdjacentMineCount_3x3_3_mines();

        //! @brief Test {@link MS::Minesweeper::getAdjacentMineCount
        //! getAdjacentMineCount} function.
        //!
        //! @test 3x3 board, 4 mines at locations 0, 1, 2, 5, testing
        //!       location index 4.<br>Adjacent mine count returns 4.
        //! @image html board_3x3_4_mines.png ""
        void testGetAdjacentMineCount_3x3_4_mines();

        //! @brief Test {@link MS::Minesweeper::getAdjacentMineCount
        //! getAdjacentMineCount} function.
        //!
        //! @test 3x3 board, 5 mines at locations 0, 1, 2, 5, 8, testing
        //!       location index 4.<br>Adjacent mine count returns 5.
        //! @image html board_3x3_5_mines.png ""
        void testGetAdjacentMineCount_3x3_5_mines();

        //! @brief Test {@link MS::Minesweeper::getAdjacentMineCount
        //! getAdjacentMineCount} function.
        //!
        //! @test 3x3 board, 6 mines at locations 0, 1, 2, 5, 8, 7,
        //!       testing location index 4.<br>Adjacent mine count
        //!       returns 6.
        //! @image html board_3x3_6_mines.png ""
        void testGetAdjacentMineCount_3x3_6_mines();

        //! @brief Test {@link MS::Minesweeper::getAdjacentMineCount
        //! getAdjacentMineCount} function.
        //!
        //! @test 3x3 board, 7 mines at locations 0, 1, 2, 5, 8, 7, 6
        //!       testing location index 4.<br>Adjacent mine count
        //!       returns 7.
        //! @image html board_3x3_7_mines.png ""
        void testGetAdjacentMineCount_3x3_7_mines();

        //! @brief Test {@link MS::Minesweeper::getAdjacentMineCount
        //! getAdjacentMineCount} function.
        //!
        //! @test 3x3 board, 8 mines at locations 0, 1, 2, 5, 8, 7, 6,
        //!       3, testing location index 4.<br>Adjacent mine count
        //!       returns 8.
        //! @image html board_3x3_8_mines.png ""
        void testGetAdjacentMineCount_3x3_8_mines();

        //! @brief Test getDistance function.
        //!
        //! @test Function getDistance returns distance between two
        //!       points on a board.
        //!
        //! @image html board_2x2.png "2x2 Board"
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 0    | 0  | 0.00000
        //! 0    | 1  | 1.00000
        //! 0    | 2  | 1.00000
        //! 0    | 3  | 1.41421
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 1    | 0  | 1.00000
        //! 1    | 1  | 0.00000
        //! 1    | 2  | 1.41421
        //! 1    | 3  | 1.00000
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 2    | 0  | 1.00000
        //! 2    | 1  | 1.41421
        //! 2    | 2  | 0.00000
        //! 2    | 3  | 1.00000
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 3    | 0  | 1.41421
        //! 3    | 1  | 1.00000
        //! 3    | 2  | 1.00000
        //! 3    | 3  | 0.00000
        void testGetDistance_2x2();

        //! @brief Test getDistance function.
        //!
        //! @test Function getDistance returns distance between two
        //!       points on a board.
        //!
        //! @image html board_3x2.png "3x2 Board"
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 0    | 0  | 0.00000
        //! 0    | 1  | 1.00000
        //! 0    | 2  | 2.00000
        //! 0    | 3  | 1.00000
        //! 0    | 4  | 1.41421
        //! 0    | 5  | 2.23606
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 1    | 0  | 1.00000
        //! 1    | 1  | 0.00000
        //! 1    | 2  | 1.00000
        //! 1    | 3  | 1.41421
        //! 1    | 4  | 1.00000
        //! 1    | 5  | 1.41421
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 2    | 0  | 2.00000
        //! 2    | 1  | 1.00000
        //! 2    | 2  | 0.00000
        //! 2    | 3  | 2.23606
        //! 2    | 4  | 1.41421
        //! 2    | 5  | 1.00000
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 3    | 0  | 1.00000
        //! 3    | 1  | 1.41421
        //! 3    | 2  | 2.23606
        //! 3    | 3  | 0.00000
        //! 3    | 4  | 1.00000
        //! 3    | 5  | 2.00000
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 4    | 0  | 1.41421
        //! 4    | 1  | 1.00000
        //! 4    | 2  | 1.41421
        //! 4    | 3  | 1.00000
        //! 4    | 4  | 0.00000
        //! 4    | 5  | 1.00000
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 5    | 0  | 2.23606
        //! 5    | 1  | 1.41421
        //! 5    | 2  | 1.00000
        //! 5    | 3  | 2.00000
        //! 5    | 4  | 1.00000
        //! 5    | 5  | 0.00000
        void testGetDistance_3x2();

        //! @brief Test getDistance function.
        //!
        //! @test Function getDistance returns distance between two
        //!       points on a board.
        //!
        //! @image html board_2x3.png "2x3 Board"
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 0    | 0  | 0.00000
        //! 0    | 1  | 1.00000
        //! 0    | 2  | 2.00000
        //! 0    | 3  | 1.00000
        //! 0    | 4  | 1.41421
        //! 0    | 5  | 2.23606
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 1    | 0  | 1.00000
        //! 1    | 1  | 0.00000
        //! 1    | 2  | 1.00000
        //! 1    | 3  | 1.41421
        //! 1    | 4  | 1.00000
        //! 1    | 5  | 1.41421
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 2    | 0  | 2.00000
        //! 2    | 1  | 1.00000
        //! 2    | 2  | 0.00000
        //! 2    | 3  | 2.23606
        //! 2    | 4  | 1.41421
        //! 2    | 5  | 1.00000
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 3    | 0  | 1.00000
        //! 3    | 1  | 1.41421
        //! 3    | 2  | 2.23606
        //! 3    | 3  | 0.00000
        //! 3    | 4  | 1.00000
        //! 3    | 5  | 2.00000
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 4    | 0  | 1.41421
        //! 4    | 1  | 1.00000
        //! 4    | 2  | 1.41421
        //! 4    | 3  | 1.00000
        //! 4    | 4  | 0.00000
        //! 4    | 5  | 1.00000
        //!
        //! from | to | returns
        //! -----|----|--------
        //! 5    | 0  | 2.23606
        //! 5    | 1  | 1.41421
        //! 5    | 2  | 1.00000
        //! 5    | 3  | 2.00000
        //! 5    | 4  | 1.00000
        //! 5    | 5  | 0.00000
        void testGetDistance_2x3();

        //! @brief Test {@link MS::Minesweeper::select select} function.
        //!
        //! @test 4x5 board, mines at locations 0, 1, 5, 12, 19. States
        //!       vector is resetted to UNKNOWN after each test.
        //!
        //! @image html board_4x5_mines_at_0_1_5_12_19.png ""
        //!
        //! Location | Revealed squares
        //! ---------|-------------------------
        //!  0       | 0
        //!  1       | 1
        //!  2       | 2
        //!  3       | 2, 3, 4, 7, 8, 9, 13, 14
        //!  4       | 2, 3, 4, 7, 8, 9, 13, 14
        //!  5       | 5
        //!  6       | 6
        //!  7       | 7
        //!  8       | 8
        //!  9       | 2, 3, 4, 7, 8, 9, 13, 14
        //! 10       | 10
        //! 11       | 11
        //! 12       | 12
        //! 13       | 13
        //! 14       | 14
        //! 15       | 10, 11, 15, 16
        //! 16       | 16
        //! 17       | 17
        //! 18       | 18
        //! 19       | 19
        void testSelect_4x5_mines_at_0_1_5_12_19();

        //! @brief Test {@link MS::Minesweeper::select select} function.
        //!
        //! @test 5x5 board, mines at locations 2, 7, 12, 17, 22 and
        //!       flags at locations 12, 13, 14. States vector is
        //!       resetted to UNKNOWN after each test.
        //!
        //! @image html
        //! board_5x5_mines_at_2_7_12_17_22_flags_at_12_13_14.png ""
        //!
        //! Location | Revealed squares
        //! ---------|-----------------------------------
        //!  0       | 0, 1, 5, 6, 10, 11, 15, 16, 20, 21
        //!  1       | 1
        //!  2       | 2
        //!  3       | 3
        //!  4       | 3, 4, 8, 9
        //!  5       | 0, 1, 5, 6, 10, 11, 15, 16, 20, 21
        //!  6       | 6
        //!  7       | 7
        //!  8       | 8
        //!  9       | 3, 4, 8, 9
        //! 10       | 0, 1, 5, 6, 10, 11, 15, 16, 20, 21
        //! 11       | 11
        //! 12       | -
        //! 13       | -
        //! 14       | -
        //! 15       | 0, 1, 5, 6, 10, 11, 15, 16, 20, 21
        //! 16       | 16
        //! 17       | 17
        //! 18       | 18
        //! 19       | 18, 19, 23, 24
        //! 20       | 0, 1, 5, 6, 10, 11, 15, 16, 20, 21
        //! 21       | 21
        //! 22       | 22
        //! 23       | 23
        //! 24       | 18, 19, 23, 24
        void testSelect_5x5_mines_at_2_7_12_17_22_flags_at_12_13_14();

        //! Test {@link MS::Minesweeper::isFinished isFinished}
        //! function.
        //!
        //! @test 1x5 board, mines at locations 1 and 3. Game finished
        //!       successfully.
        //!
        //! @image html board_1x5_mines_at_1_3.png ""
        //!
        //! select | isFinished
        //! -------|-----------
        //! 0      | false
        //! 2      | false
        //! 4      | true
        void testIsFinished_success();

        //! Test {@link MS::Minesweeper::isFinished isFinished}
        //! function.
        //!
        //! @test 1x5 board, mines at locations 1 and 3. Game finished
        //!       unsuccessfully.
        //!
        //! @image html board_1x5_mines_at_1_3.png ""
        //!
        //! select | isFinished
        //! -------|-----------
        //! 0      | false
        //! 2      | false
        //! 1      | true
        void testIsFinished_failure();

        //! Test {@link MS::Minesweeper::isFinished isFinished}
        //! function.
        //!
        //! @test 2x2 board, mines at location 1. Click 3, 2. Set flag
        //! 1. Click 0. Game finished successfully.
        //!
        //! action        | isFinished
        //! --------------|-----------
        //! 3             | false
        //! 2             | false
        //! set flag at 1 | false
        //! 0             | true
        void testIsFinished_2x2_mines_at_1();

        //! @brief Test actual game play.
        //!
        //! @test 8x8 board, 10 mines. See game play actions in table
        //!       @ref actionsTable_0 "below".
        //! <ul>
        //! <li>States vector is correctly initialized</li>
        //! <li>Adjacent mine count at each board location index</li>
        //! <li>Function select returns correct values at each given
        //!     location</li>
        //! <li>Function select modifies states vector</li>
        //! <li>Function isFinished functionality
        //! </ul>
        //!
        //! @image html board_8x8_game_0.png
        //!
        //! | Mine locations                        |
        //! |---------------------------------------|
        //! | 4, 16, 26, 32, 42, 47, 49, 52, 58, 59 |
        //!
        //! | Adjacent Mine Count    |
        //! |------------------------|
        //! | 0, 0, 0, 1, 0, 1, 0, 0 |
        //! | 1, 1, 0, 1, 1, 1, 0, 0 |
        //! | 0, 2, 1, 1, 0, 0, 0, 0 |
        //! | 2, 3, 0, 1, 0, 0, 0, 0 |
        //! | 0, 3, 2, 2, 0, 0, 1, 1 |
        //! | 2, 3, 1, 2, 1, 1, 1, 0 |
        //! | 1, 2, 4, 4, 1, 1, 1, 1 |
        //! | 1, 2, 2, 2, 2, 1, 0, 0 |
        //! @anchor actionsTable_0
        //! Action | Location | Finished | Function select returns
        //! -------|----------|----------|------------------------
        //! REVEAL | 1        | false    | 1, 0, 2, 9, 8, 10, 3, 17, 11, 18, 19
        //! REVEAL | 20       | false    | 20, 12, 21, 28, 13, 27, 29, 22, 36, 14, 30, 35, 37, 38, 5, 6, 23, 44, 15, 31, 43, 45, 7, 39, 46
        //! REVEAL | 34       | false    | 34
        //! REVEAL | 25       | false    | 25
        //! REVEAL | 24       | false    | 24
        //! REVEAL | 33       | false    | 33
        //! REVEAL | 41       | false    | 41
        //! REVEAL | 40       | false    | 40
        //! REVEAL | 55       | false    | 55
        //! REVEAL | 54       | false    | 54
        //! REVEAL | 63       | false    | 63, 62, 61, 53
        //! REVEAL | 51       | false    | 51
        //! REVEAL | 60       | false    | 60
        //! REVEAL | 58       | false    | 58
        //! REVEAL | 56       | false    | 56
        //! REVEAL | 48       | false    | 48
        //! REVEAL | 57       | true     | 57
        void testGame_8x8_0();

        //! @brief Test actual game play.
        //!
        //! @test 8x8 board, 10 mines. See game play actions in table
        //!       @ref actionsTable_1 "below".
        //! <ul>
        //! <li>States vector is correctly initialized</li>
        //! <li>Adjacent mine count at each board location index</li>
        //! <li>Function select returns correct values at each given
        //!     location</li>
        //! <li>Function select modifies states vector</li>
        //! <li>Function isFinished functionality
        //! </ul>
        //!
        //! @image html board_8x8_game_1.png
        //!
        //! | Mine locations                        |
        //! |---------------------------------------|
        //! | 0, 7, 12, 23, 24, 25, 29, 38, 53, 59  |
        //!
        //! | Adjacent Mine Count    |
        //! |------------------------|
        //! | 0, 1, 0, 1, 1, 1, 1, 0 |
        //! | 1, 1, 0, 1, 0, 1, 2, 2 |
        //! | 2, 2, 1, 1, 2, 2, 2, 0 |
        //! | 1, 1, 1, 0, 1, 1, 3, 2 |
        //! | 2, 2, 1, 0, 1, 2, 1, 1 |
        //! | 0, 0, 0, 0, 1, 2, 2, 1 |
        //! | 0, 0, 1, 1, 2, 0, 1, 0 |
        //! | 0, 0, 1, 0, 2, 1, 1, 0 |
        //! @anchor actionsTable_1
        //! Action | Location | Finished | Function select returns
        //! -------|----------|----------|------------------------
        //! REVEAL | 10       | false    | 10, 2, 9, 11, 18, 1, 3, 17, 19
        //! REVEAL | 20       | false    | 20
        //! TOGGLE | 48       | false    | -
        //! TOGGLE | 49       | false    | -
        //! REVEAL | 16       | false    | 16
        //! REVEAL | 26       | false    | 26
        //! REVEAL | 34       | false    | 34
        //! REVEAL | 33       | false    | 33
        //! REVEAL | 35       | false    | 35, 27, 36, 43, 28, 42, 44, 51, 41, 50, 52, 32, 40
        //! REVEAL | 8        | false    | 8
        //! REVEAL | 4        | false    | 4
        //! REVEAL | 5        | false    | 5
        //! REVEAL | 13       | false    | 13
        //! REVEAL | 21       | false    | 21
        //! REVEAL | 14       | false    | 14
        //! REVEAL | 6        | false    | 6
        //! TOGGLE | 48       | false    | -
        //! REVEAL | 48       | false    | 48, 56, 57, 58
        //! TOGGLE | 49       | false    | -
        //! REVEAL | 49       | false    | 49
        //! REVEAL | 22       | false    | 22
        //! REVEAL | 30       | false    | 30
        //! REVEAL | 31       | false    | 31
        //! REVEAL | 15       | false    | 15
        //! REVEAL | 37       | false    | 37
        //! REVEAL | 45       | false    | 45
        //! REVEAL | 63       | false    | 63, 55, 62, 54, 47, 46
        //! REVEAL | 39       | false    | 39
        //! REVEAL | 61       | false    | 61
        //! REVEAL | 60       | true     | 60
        void testGame_8x8_1();

    private:
        //! @brief Game test actions
        //!
        //! See @ref testGame_8x8_0 for details.
        enum Actions
        {
            REVEAL, //!< Reveal square
            TOGGLE  //!< Toggle flag
        };

        //! @brief Game test action data
        //!
        //! Test inputs (action and location) and test output (finished
        //! and selection) data.
        struct TestAction
        {
            // Input data
            Actions action; //!< Current action
            unsigned int location; //!< Location index
            // Expected results
            bool finished; //!< Should game be finished after the action
            //! Expected select function result for the current action
            std::vector<unsigned int> selection;
        };

        MS::Minesweeper* ms_;

        // Helper methods

        //! @brief Mine array tests.
        //!
        //! Mine array contains valid mine locations. See @ref
        //! testSetMines for details.
        //!
        //! @param rows Number of rows on board
        //! @param cols Number of columns on board
        //! @param mines Number of mines on board
        //! @param first First selected location
        void mineArrayTests(
            unsigned int rows,
            unsigned int cols,
            unsigned int mines,
            unsigned int first
        );

        //! @brief Setup function tests.
        //!
        //! Board setup validation. See @ref testSetup for details.
        //!
        //! @param rows Number of rows on board
        //! @param cols Number of columns on board
        //! @param mines Number of mines on board
        void setupTests(
            unsigned int rows,
            unsigned int cols,
            unsigned int mines
        );

        //! @brief isMined function tests.
        //!
        //! See @ref testIsMined for details.
        //!
        //! @param rows Number of rows on board
        //! @param cols Number of cols on board
        //! @param mines Array of mine locations
        void isMinedTests(
            unsigned int rows,
            unsigned int cols,
            std::vector<unsigned int> mines
        );

        //! @brief getAdjacentSquares function tests.
        //!
        //! @see @ref testGetAdjacentSquares_3x3_at_0,
        //! @ref testGetAdjacentSquares_3x3_at_1,
        //! @ref testGetAdjacentSquares_3x3_at_2,
        //! @ref testGetAdjacentSquares_3x3_at_3,
        //! @ref testGetAdjacentSquares_3x3_at_4,
        //! @ref testGetAdjacentSquares_3x3_at_5,
        //! @ref testGetAdjacentSquares_3x3_at_6,
        //! @ref testGetAdjacentSquares_3x3_at_7,
        //! @ref testGetAdjacentSquares_3x3_at_8 for details.
        //!
        //! @param rows Number of rows on board
        //! @param cols Number of cols on board
        //! @param location Index for which adjacent squares are
        //!        searched for
        //! @param expected Expected values which function should return
        void getAdjacentSquaresTests(
            unsigned int rows,
            unsigned int cols,
            unsigned int location,
            std::vector<unsigned int> expected
        );

        //! brief getAdjacentMineCount function tests.
        //!
        //! See @ref testGetAdjacentMineCount_3x3_0_mines,
        //! @ref testGetAdjacentMineCount_3x3_1_mines,
        //! @ref testGetAdjacentMineCount_3x3_2_mines,
        //! @ref testGetAdjacentMineCount_3x3_3_mines,
        //! @ref testGetAdjacentMineCount_3x3_4_mines,
        //! @ref testGetAdjacentMineCount_3x3_5_mines,
        //! @ref testGetAdjacentMineCount_3x3_6_mines,
        //! @ref testGetAdjacentMineCount_3x3_7_mines,
        //! @ref testGetAdjacentMineCount_3x3_8_mines for details.
        //!
        //! @param rows Number of rows on board
        //! @param cols Number of cols on board
        //! @param location Index for which adjacent mine count is
        //!        searched for
        //! @param mines Mine locations
        void getAdjacentMineCountTests(
            unsigned int rows,
            unsigned int cols,
            unsigned int location,
            std::vector<unsigned int> mines
        );

        //! @brief getDistance function tests.
        //!
        //! See @ref testGetDistance_2x2,
        //! @ref testGetDistance_3x2,
        //" @ref testGetDistance_2x3 for details.
        //!
        //! @param rows Number of rows on board
        //! @param cols Number of cols on board
        //! @param expected Expected values which function should return
        void getDistanceTests(
            unsigned int rows,
            unsigned int cols,
            const std::vector<std::vector<double> >& expected
        );

        //! @brief select function tests.
        //!
        //! See @ref testSelect_mines_at_4x5_mines_at_0_1_5_12_19,
        //! @ref testSelect_mines_at_2_7_12_17_22_flags_at_12_13_14
        //! for details.
        //!
        //! @param rows Number of rows on board
        //! @param cols Number of cols on board
        //! @param mines Array of mine locations
        //! @param flags Array of flag locations
        //! @param expected Expected values which function should return
        void selectTests(
            unsigned int rows,
            unsigned int cols,
            const std::vector<unsigned int>& mines,
            const std::vector<unsigned int>& flags,
            const std::vector<std::vector<unsigned int> >& expected
        );

        //! @brief isFinished function tests.
        //!
        //! See @ref testIsFinished_success, @ref testIsFinished_failure
        //! for details.
        //!
        //! @param rows Number of rows on board
        //! @param cols Number of cols on board
        //! @param mines Array of mine locations
        //! @param data Array of pairs of selected locations and
        //!        respective isFinished function return values
        void isFinishedTests(
            unsigned int rows,
            unsigned int cols,
            const std::vector<unsigned int>& mines,
            const std::vector<std::pair<unsigned int, bool> > data
        );

        //! @brief Game play tests.
        //!
        //! See @ref testGame_8x8_0, @ref testGame_8x8_1 for details.
        //!
        //! @param rows Number of rows on board
        //! @param cols Number of cols on board
        //! @param mines Array of mine locations
        //! @param adjacentMines Array of adjacent mine counts at each
        //!        location index
        //! @param actions Array of test actions
        void gameTests(
            unsigned int rows,
            unsigned int cols,
            const std::vector<unsigned int>& mines,
            const std::vector<unsigned int>& adjacentMines,
            std::vector<TestAction>& actions
        );

        //! @brief Convert std::vector contents into a string
        //!
        //! @param v vector
        //! @return comma separated list of vector contents
        std::string vec2str(const std::vector<unsigned int>& v) const;
    };
}

//! @brief Overload << operator to print state name.
//!
//! @param os Original output stream object to modify
//! @param state State enumeration
//! @return Modified output stream object
std::ostream& operator<<(
    std::ostream& os,
    const MS::Minesweeper::States& state
);

#endif // TEST_MINESWEEPER_H
